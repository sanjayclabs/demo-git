var mysql = require('mysql'),
    DBSettings = require('../config/ApplicationSettings').getDBSettings();

//database connection

connection = mysql.createConnection({
    host: DBSettings.host,
    user: DBSettings.username,
    password: DBSettings.password,
    database: DBSettings.database
});
connection.connect();