var func = require('./commonfunction');
var sendResponse = require('./sendResponse');
var async = require('async');


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Apply promo code
 * INPUT : accessToken,promoCode
 * OUTPUT : if promo code is valid or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.ApplyPromotionCode = function(req, res) {

    var accessToken = req.body.access_token;
    var promoCode = req.body.promo_code;

    console.log(req.body);
    var manValues = [accessToken, promoCode];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {

            var extraDataNeeded = ['credits'];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        },
        function(userInfo, callback) {
            checkPromoCode(promoCode, userInfo[0].user_id, res, function(codeId,credits) {
                callback(userInfo, codeId, credits);
            });
        }], function(userInfo, codeId, credits) {


        var sql = "INSERT INTO `user_codes_applied`(`user_id`, `code_id`) VALUES (?,?)";
        connection.query(sql, [userInfo[0].user_id, codeId], function(err, responseInsert) {
            console.log(err)
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                var sql = "UPDATE `users` SET `credits`=`credits`+? WHERE `user_id`=? limit 1";
                connection.query(sql, [credits, userInfo[0].user_id], function(err, responseUpdateCredits) {
                    var finalCredits = credits + userInfo[0].credits;
                var data = {"message": "Promotion code applied successfully.","credits":finalCredits};
                sendResponse.sendSuccessData(data, res);
                });
            }
        });
    }
    );
};

function checkPromoCode(promoCode, userId, res, callback) {

    var sql = "SELECT `code_id`, `credits` FROM `promotion_codes` WHERE `code`=? AND CURDATE() BETWEEN `start_date` AND `expiry_date` limit 1";
    connection.query(sql, [promoCode], function(err, responseCode ){

        if (responseCode.length == 1) {

            var sql = "SELECT `id` FROM `user_codes_applied` WHERE `user_id`=? AND `code_id`=? limit 1";
            connection.query(sql, [userId, responseCode[0].code_id], function(err, responseUserCode) {
                if (responseUserCode.length == 1) {
                    
                    var error = 'Promotion code already applied before.';
                    sendResponse.sendError(error, res);

                }
                else {

                    callback(responseCode[0].code_id,responseCode[0].credits);
               }
            });
        }
        else {
            var error = 'Incorrect promotion code.';
            sendResponse.sendError(error, res);
        }

    });


}