module.exports = {
    "production": {
        "port" : "2001",
        "iosGateway" : "gateway.push.apple.com",
        "pemFile" : "vizavoo_tech.pem",
        "androidKey" : "AIzaSyCDjqmu2RZhPwwdJYTcz0lEzPEpQqfnyzE",
        "servicePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/service/",
        "profilePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/profile/",
        "stripeToken" : "sk_test_AXFBg4piX0Bbom8zKliOMLTn",
        "domainName" : "live.vizavoo.com"
    },
    "test": {
        "port" : "2001",
        "iosGateway" : "gateway.push.apple.com",
        "pemFile" : "vizavoo_tech.pem",
        "androidKey" : "AIzaSyCDjqmu2RZhPwwdJYTcz0lEzPEpQqfnyzE",
        "servicePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/service/",
        "profilePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/profile/",
        "stripeToken" : "sk_test_AXFBg4piX0Bbom8zKliOMLTn",
        "domainName" : "api.vizavoo.com"
    },
    "development": {
        "port" : "2001",
        "iosGateway" : "gateway.push.apple.com",
        "pemFile" : "vizavoo_tech.pem",
        "androidKey" : "AIzaSyCDjqmu2RZhPwwdJYTcz0lEzPEpQqfnyzE",
        "servicePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/service/",
        "profilePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/profile/",
        "stripeToken" : "sk_test_AXFBg4piX0Bbom8zKliOMLTn",
        "domainName" : "api.vizavoo.com"

    }
};
