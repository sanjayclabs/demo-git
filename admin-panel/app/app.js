(function (window) {

    var app = angular.module('customersApp', ['ngRoute', 'ngCookies', 'ui.bootstrap', 'angularFileUpload']);

    app.constant("MY_CONSTANT", {
        "url": "http://54.153.1.114:2500",
        "HAIR": "Hair",
        "MAKEUP": "Make-up",
        "HAIRVALUE": "1",
        "MAKEUPVALUE": "2"
    });

    app.directive('validNumber', function() {
        return {
            require: '?ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
                if(!ngModelCtrl) {
                    return;
                }

                ngModelCtrl.$parsers.push(function(val) {
                    if (angular.isUndefined(val)) {
                        var val = '';
                    }
                    var clean = val.replace(/\D/g, '');
                    clean = clean.replace(/^[0]/,'');

                    var decimalCheck = clean.split('.');

                    if(!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0,2);
                        clean =decimalCheck[0] + '.' + decimalCheck[1];
                    }

                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });

                element.bind('keypress', function(event) {
                    if(event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    });

    app.directive('first', function (MY_CONSTANT) {
        return {
            restrict: "E",
            scope: "=",
            link: function ($scope) {
                $scope.uploadFile = function (token, id, cat, name, desc, costt, timee, files) {


                    var bool;
                    if (files != null) {
                        bool = 1;
                        var file = files[0];

                    } else {
                        bool = 0;

                    }
                    if (cat == MY_CONSTANT.HAIR) {
                        cat = MY_CONSTANT.HAIRVALUE;
                    }
                    if (cat == MY_CONSTANT.MAKEUP) {
                        cat = MY_CONSTANT.MAKEUPVALUE;
                    }

                    var formData = new FormData();

                    formData.append('access_token', token);
                    formData.append('category', cat);
                    formData.append('service_id', id);
                    formData.append('service_name', name);
                    formData.append('description', desc);
                    formData.append('cost', costt);
                    formData.append('time', timee);
                    formData.append('service_image', file);
                    formData.append('image_sent', bool);

                    $.ajax({
                        type: "POST",
                        url: MY_CONSTANT.url + '/edit_service',
                        dataType: "json",
                        data: formData,

                        async: false,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            console.log(data);

                            $scope.data = data;
                            if (data.log) {

                                location.reload();

                            }

                        }


                    });


                }

            }


        }
    });
    app.directive('second', function (MY_CONSTANT) {
        return {
            restrict: "E",
            scope: "=",
            link: function ($scope) {
                $scope.addFile = function (token, cat, name, desc, costt, timee, files) {

                    console.log(cat);

                    var file = files[0];

                    if (cat == MY_CONSTANT.HAIR) {
                        cat = MY_CONSTANT.HAIRVALUE;
                    }
                    if (cat == MY_CONSTANT.MAKEUP) {
                        cat = MY_CONSTANT.MAKEUPVALUE;
                    }
                    console.log(token);


                    var formData = new FormData();


                    formData.append('access_token', token);
                    formData.append('category', cat);
                    formData.append('service_name', name);
                    formData.append('description', desc);
                    formData.append('cost', costt);
                    formData.append('time', timee);
                    formData.append('service_image', file);


                    $.ajax({
                        type: "POST",
                        url: MY_CONSTANT.url + '/add_new_service',
                        dataType: "json",
                        data: formData,

                        async: false,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            console.log(data);
                            $scope.data = data;
                            if (data.log) {
                                location.reload();
                            }
                        }

                    })

                }
            }


        }
    });

    app.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.


                when('/login', {
                    title: 'Login',
                    controller: 'LoginController',
                    templateUrl: 'app/views/login_user.html'
                })
                .when('/resetpassword', {
                    title: 'Reset Password',
                    controller: 'LoginController',
                    templateUrl: 'app/views/reset.html'
                })

                .when('/customerinfo/:customerId', {
                    title: 'Customer Information',
                    templateUrl: 'app/views/customer_info.html',
                    controller: 'CustomerViewController'
                })
                .when('/artistinfo/:artistId', {
                    title: 'Artist Information',
                    templateUrl: 'app/views/artist_info.html',
                    controller: 'ArtistViewController'
                })
                .when('/logout', {
                    title: 'Logout',
                    templateUrl: 'header.html',
                    controller: 'LogoutController'
                })
                .when('/dashboard', {
                    title: 'Dashboard',
                    templateUrl: 'app/views/dashboard.html',
                    controller: 'DashboardController'
                })
                .when('/upcoming_session', {
                    title: 'Upcoming Session',
                    templateUrl: 'app/views/upcoming_session.html',
                    controller: 'UpcomingSessionController'
                })
                .when('/past_session', {
                    title: 'Past Session',
                    templateUrl: 'app/views/past_session.html',
                    controller: 'PastSessionController'
                })
                .when('/partybooking', {
                    title: 'Party Booking',
                    templateUrl: 'app/views/party_booking.html',
                    controller: 'PartyBookingController'
                })

                .when('/verified_artists', {
                    title: 'Verified Artists',
                    templateUrl: 'app/views/verified_artists.html',
                    controller: 'ArtistController'
                })
                .when('/new_artists', {
                    title: 'New Request Artists',
                    templateUrl: 'app/views/new_artists.html',
                    controller: 'VerifyArtistController'
                })
                .when('/services', {
                    title: 'Services',
                    templateUrl: 'app/views/services.html',
                    controller: 'ServiceController'
                })
                .when('/reports', {
                    title: 'Reports',
                    templateUrl: 'app/views/reports.html',
                    controller: 'ReportsController'
                })
                .when('/payments', {
                    title: 'Payments',
                    templateUrl: 'app/views/payments.html',
                    controller: 'PaymentsController'
                })
                .otherwise({
                    redirectTo: '/login'
                });

        }]);
    window.app = app;

}(window));




 
