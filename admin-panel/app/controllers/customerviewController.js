app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }


});

app.controller('CustomerViewController', function ($scope, $http, $route, $routeParams, $cookieStore, $timeout, MY_CONSTANT) {


    if (typeof $cookieStore.get('obj') == "undefined") {
        window.location.href = "#/login";
        return;
    }

    $scope.currentPage = 1; //current page
    $scope.entryLimit = 5;
    var userId = $routeParams.customerId;
    console.log("here");
    $.post(MY_CONSTANT.url + '/customer_details',
        {

            access_token: $cookieStore.get('obj').accesstoken,
            user_id: userId
        },


        function (data) {

            console.log(data);


            var dataArray = [];

            data = JSON.parse(data);

            $scope.name = data.customer_detail[0].name;
            $scope.email = data.customer_detail[0].email;
            $scope.mobile = data.customer_detail[0].mobile;
            $scope.image_url = data.customer_detail[0].image_url;

            data.services.forEach(function (column) {

                var d = {

                    booking_id: "",
                    id: "",
                    address: "",
                    artist_name: "",
                    service_date: "",
                    start_time: "",
                    end_time: "",
                    rating: "",
                    cost: ""

                };

                var date = column.service_date.toString().split("T")[0];
                d.booking_id = column.booking_id;
                d.id = column.id;

                d.artist_name = column.artist_name;
                d.start_time = column.start_time;
                d.end_time = column.end_time;
                d.address = column.address;
                d.service_date = date;
                d.rating = column.rating;
                d.cost = column.cost;
                dataArray.push(d);


            });

            $scope.$apply(function () {
                $scope.list1 = dataArray;
                $scope.filteredItems = $scope.list1.length; //Initially for no filter
                $scope.totalItems = $scope.list1.length;
            });


            console.log(data);


        });


    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function () {
        $timeout(function () {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };


});

        