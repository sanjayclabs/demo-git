app.controller('ReportsController', function ($scope, $cookieStore, $location, $timeout, MY_CONSTANT) {

    if (typeof $cookieStore.get('obj') == "undefined") {
        window.location.href = "#/login";
        return;
    }
    $scope.options = [{
        name: 'Customer',
        value: '1'
    }, {
        name: 'Artist',
        value: '2'
    }, {
        name: 'Service',
        value: '3'
    }, {
        name: 'Session',
        value: '4'
    }];

    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return "active"
        } else {
            return ""
        }
    };

    $scope.start = new Date('11/20/14');
    $scope.end = new Date();

    $scope.minStartDate = 0; //fixed date
    $scope.maxStartDate = $scope.end; //init value
    $scope.minEndDate = $scope.start; //init value
    $scope.maxEndDate = $scope.end; //fixed date same as $scope.maxStartDate init value

    $scope.$watch('start', function (v) {
        $scope.minEndDate = v;
    });
    $scope.$watch('end', function (v) {
        $scope.maxStartDate = v;
    });

    $scope.openStart = function () {
        $timeout(function () {
            $scope.startOpened = true;
        });
    };

    $scope.openEnd = function () {
        $timeout(function () {
            $scope.endOpened = true;
        });
    };

    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1
    };

    $scope.addReport = function (reportdata, date1, date2) {

        if (reportdata == 'Customer') {
            reportdata = 1;
        }
        if (reportdata == 'Artist') {
            reportdata = 2;
        }
        if (reportdata == 'Service') {
            reportdata = 3;
        }
        if (reportdata == 'Session') {
            reportdata = 4;
        }
        console.log(reportdata);
        console.log(date1);
        console.log(date2);
        $.post(MY_CONSTANT.url + '/download_report',
            {


                access_token: $cookieStore.get('obj').accesstoken,
                report_of: reportdata,
                date_from: date1,
                date_to: date2


            }
            ,

            function (result) {

                console.log(result);
                //$scope.is_block = data;
                var data = result;
                if (data == '')
                    return;

                JSONToCSVConvertor(data, "Report", true);


            });


    };

});

function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "Vizavoo_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}