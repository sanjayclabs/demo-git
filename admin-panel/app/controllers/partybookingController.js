app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }


});
app.controller('PartyBookingController', function ($scope, $http, $route, $cookies, $cookieStore, $timeout, $location, MY_CONSTANT) {

    if (typeof $cookieStore.get('obj') == "undefined") {
        window.location.href = "#/login";
        return;
    }

    $scope.currentPage = 1; //current page
    $scope.entryLimit = 5;

    $.post(MY_CONSTANT.url + '/party_booking', {

        access_token: $cookieStore.get('obj').accesstoken


    }, function (data) {


        console.log(data);
        var dataArray = [];

        data = JSON.parse(data);

        data.forEach(function (column) {

            var d = {

                party_booking_id: "",
                customer_name: "",
                mobile: "",
                party_name: "",
                guest_count: "",
                party_date: "",
                appointment_time: ""


            };


            var date = column.party_date.toString().split("T")[0];
            d.party_booking_id = column.party_booking_id;
            d.customer_name = column.customer_name;
            d.mobile = column.mobile;
            d.party_name = column.party_name;
            d.guest_count = column.guest_count;
            d.appointment_time = column.appointment_time;
            d.party_date = date;


            console.log(d);
            dataArray.push(d);


        });

        $scope.$apply(function () {
            $scope.list = dataArray;
            $scope.filteredItems = $scope.list.length; //Initially for no filter
            $scope.totalItems = $scope.list.length;
        });


        console.log(data);


    });


    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return "active"
        } else {
            return ""
        }
    };

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function () {
        $timeout(function () {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };


});


