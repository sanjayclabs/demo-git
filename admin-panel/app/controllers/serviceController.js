app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }


});
app.controller('ServiceController', function ($scope, $http, $cookieStore, $timeout, $location, MY_CONSTANT) {
    if (typeof $cookieStore.get('obj') == "undefined") {
        window.location.href = "#/login";
        return;
    }

    $scope.options = [{
        name: MY_CONSTANT.HAIR,
        value: MY_CONSTANT.HAIRVALUE
    }, {
        name: MY_CONSTANT.MAKEUP,
        value: MY_CONSTANT.MAKEUPVALUE
    }];


    $scope.currentPage = 1; //current page
    $scope.entryLimit = 5;
    $scope.token = $cookieStore.get('obj').accesstoken;
    $.post(MY_CONSTANT.url + '/services', {

        access_token: $cookieStore.get('obj').accesstoken


    }, function (data) {


        console.log(data);
        var dataArray = [];
        data = JSON.parse(data);
        data.forEach(function (column) {


            var d = {

                service_id: "",
                category: "",
                name: "",
                image_url: "",
                description: "",
                time: "",
                cost: ""


            };


            d.service_id = column.service_id;
            d.category = column.category;

            d.name = column.name;
            d.image_url = column.image_url;
            d.description = column.description;
            d.time = column.time;
            d.cost = column.cost;
            dataArray.push(d);
            $scope.category = column.category;

        });

        $scope.$apply(function () {

            $scope.list = dataArray;
            $scope.filteredItems = $scope.list.length; //Initially for no filter
            $scope.totalItems = $scope.list.length;
        });

    });


    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return "active"
        } else {
            return ""
        }
    };
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function () {
        $timeout(function () {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };


    $scope.editService = function (serviceId, cat) {
        if (cat == MY_CONSTANT.HAIRVALUE) {
            $scope.category = MY_CONSTANT.HAIR;
        }
        if (cat == MY_CONSTANT.MAKEUPVALUE) {
            $scope.category = MY_CONSTANT.MAKEUP;
        }
        if (cat == '0') {
            $scope.category = '';
        }


        $scope.service_id = serviceId;
        $.post(MY_CONSTANT.url + '/pre_edit_service_details',
            {


                access_token: $cookieStore.get('obj').accesstoken,
                service_id: serviceId

            }
            ,

            function (data) {

                console.log(data);
                //$scope.is_block = data;
                data = JSON.parse(data);
                $scope.$apply(function () {

                    $scope.data11 = data;


                });


            });

    };


});
 

        

