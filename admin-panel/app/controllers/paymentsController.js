

app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }


});
app.controller('PaymentsController', function ($scope, $http, $route, $cookies, $cookieStore, $timeout, $location, MY_CONSTANT) {

    if (typeof $cookieStore.get('obj') == "undefined") {
        window.location.href = "#/login";
        return;
    }

    $scope.amount = {};

    $scope.makePayment = function (artistId, unpaid) {
        console.log(artistId);

        if ($scope.amount.x > unpaid) {
            alert("Amount value will not exceed unpaid amount value");
        }
        else {
            $.post(MY_CONSTANT.url + '/pay_to_artist',
                {


                    access_token: $cookieStore.get('obj').accesstoken,
                    artist_id: artistId,
                    amount: $scope.amount.x,
                    statement_description: $scope.amount.y

                }
                ,

                function (data) {

                    data = JSON.parse(data);
                    console.log(data);
                    if(data.error)
                    {
                        alert(data.error);
                    }
                    else
                    {
                        alert(data.log);
                    }

                });

        }


    };

    $scope.currentPage = 1; //current page
    $scope.entryLimit = 5;

    $.post(MY_CONSTANT.url + '/artist_payment_detail', {

        access_token: $cookieStore.get('obj').accesstoken


    }, function (data) {


        console.log(data);
        var dataArray = [];

        data = JSON.parse(data);


        data.forEach(function (column) {

            var d = {

                artist_id: "",
                artist_name: "",
                paid_amount: "",
                unpaid_amount: ""


            };


            d.artist_id = column.artist_id;
            d.artist_name = column.artist_name;
            d.paid_amount = column.paid_amount;
            d.unpaid_amount = column.unpaid_amount;


            console.log(d);
            dataArray.push(d);


        });

        $scope.$apply(function () {
            $scope.list = dataArray;
            $scope.filteredItems = $scope.list.length; //Initially for no filter
            $scope.totalItems = $scope.list.length;
        });


        console.log(data);


    });


    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return "active"
        } else {
            return ""
        }
    };
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function () {
        $timeout(function () {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };


});


