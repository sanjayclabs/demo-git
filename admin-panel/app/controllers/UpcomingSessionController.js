app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }


});
app.controller('UpcomingSessionController', function ($scope, $http, $route, $cookies, $cookieStore, $timeout, $location, MY_CONSTANT) {

    if (typeof $cookieStore.get('obj') == "undefined") {
        window.location.href = "#/login";
        return;
    }

    $scope.currentPage = 1; //current page
    $scope.entryLimit = 5;

    $.post(MY_CONSTANT.url + '/upcoming_booking', {

        access_token: $cookieStore.get('obj').accesstoken


    }, function (data) {


        console.log(data);
        var dataArray = [];

        data = JSON.parse(data);

        data.forEach(function (column) {

            var d = {

                id: "",
                booking_id: "",
                service_id: "",
                technician_id: "",
                address: "",
                customer_name: "",
                artist_name: "",
                service_date: "",
                start_time: "",
                status: "",
                cost: "",
                category: "",
                service_name: ""
            };

            var date = column.service_date.toString().split("T")[0];
            d.id = column.id;
            d.booking_id = column.booking_id;
            d.service_id = column.service_id;
            d.technician_id = column.technician_id;
            d.address = column.address;
            d.customer_name = column.customer_name;
            d.artist_name = column.artist_name;

            d.start_time = column.start_time;
            d.cost = column.cost;
            d.category = column.category;
            d.service_date = date;
            d.status = column.status;
            d.service_name = column.service_name;

            dataArray.push(d);


        });

        $scope.$apply(function () {
            $scope.list = dataArray;
            $scope.filteredItems = $scope.list.length; //Initially for no filter
            $scope.totalItems = $scope.list.length;
        });


        console.log(data);


    });

    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return "active"
        } else {
            return ""
        }
    };

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function () {
        $timeout(function () {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };


});


