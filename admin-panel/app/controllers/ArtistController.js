app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
app.controller('ArtistController', function ($scope, $http, $route, $cookies, $cookieStore, $timeout, $location, MY_CONSTANT) {


    if (typeof $cookieStore.get('obj') == "undefined") {
        window.location.href = "#/login";
        return;
    }
    $scope.currentPage = 1; //current page
    $scope.entryLimit = 5;

    $.post(MY_CONSTANT.url + '/verified_artists', {

        access_token: $cookieStore.get('obj').accesstoken


    }, function (data) {

        var dataArray = [];

        data = JSON.parse(data);


        data.forEach(function (column) {

            var d = {

                user_id: "",
                user_name: "",
                email: "",
                mobile: "",
                qualification: "",
                rating: "",
                zipcode: "",
                registration_datetime: "",
                is_block: ""
            };

            var date = column.registration_datetime.toString().split("T")[0];
            d.user_id = column.user_id;
            d.user_name = column.first_name + " " + column.last_name;

            d.email = column.email;
            d.mobile = column.mobile;
            d.qualification = column.qualification;
            d.rating = column.rating;
            d.zipcode = column.zipcode;
            d.registration_datetime = date;
            d.is_block = column.is_block;
            dataArray.push(d);
        });

        $scope.$apply(function () {
            $scope.list = dataArray;
            $scope.filteredItems = $scope.list.length; //Initially for no filter
            $scope.totalItems = $scope.list.length;
        });

    });


    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return "active"
        } else {
            return ""
        }
    };
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function () {
        $timeout(function () {
            $scope.filteredItems = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };

    // for block or unblock
    $scope.reloadRoute = function () {
        $route.reload();
    };

    $scope.changeStatus = function (status, artistId) {
        console.log(status);
        $.post(MY_CONSTANT.url + '/block_unblock_artist',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                artist_id: artistId,
                new_block_status: status
            }
            ,
            function (data) {

                console.log(data);
            });
    };

});
 

        

