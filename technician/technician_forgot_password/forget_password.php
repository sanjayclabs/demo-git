<?php
$token = $_GET['token'];
if(empty($token)) {
    header("location:invalid_token.php");
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            Reset Password
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="bootstrap/bootstrap.css" rel="stylesheet">
        <link href="bootstrap/bootstrap-responsive.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        
        <link rel="stylesheet" type="text/css" href="bootstrap/bootstrap.min.css">
    </script>

<script>
    function checkPasswordMatch() {
        var password = $("#new_password").val();
        var confirmPassword = $("#confirm_password").val();

        if (password != confirmPassword) {
            $("#divCheckPasswordMatch").html("Passwords do not match!");
            $("#submit").attr('disabled', 'disabled');
        }
        else {
            $("#divCheckPasswordMatch").html("Passwords match.");
            $("#submit").removeAttr('disabled');
        }
    }

    $(document).ready(function() {
        var url = window.location.href;
        var string = url.split("=");
        var token = string[1];
        
        $.ajax({
                type: 'POST',
                url: 'http://api.vizavoo.com:3001/check_token',
                dataType: "json",
                data: 'token=' + token,
                success: function(json) {
                    if (json['error']) {
                        window.location.href="invalid_token.php";
                    }
                }
            });
        
        $("#confirm_password").keyup(checkPasswordMatch);
        $('#submit').click(function() {
            var password = $('#new_password').val();
            $.ajax({
                type: 'POST',
                url: 'http://api.vizavoo.com:3001/set_password',
                dataType: "json",
                data: 'token=' + token + '&password=' + password,
                success: function(json) {
                    console.log(json);
                    if(json['log']) {
                        $('#new_password').val("");
                        $('#confirm_password').val("");
                        $("#divCheckPasswordMatch").html("");
                        window.location.href="success_password.php";
                    }
                    
                    if (json['error']) {
                       alert(json['error']);
                    }
                    
                }
            });
           
        });
    }); 

</script>

</head>
<body class="background" style="margin-top:100px;background-repeat: no-repeat;background-size: cover;background-color: #f5efe6;"  >
    <div  class="" style="margint-top: 106px;
          width: 400px;
          background-color:rgba(256,256,256,0.7);
          margin: auto;
          border-radius: 17px;
          margin: auto;height:220px;">
        <form id="submitForm" style="margin-left:75px" method="post" class="form-signin">
            <h2 class="form-signin-heading">
                Reset Password
            </h2>
            <input type="password" id="new_password" class="input-block-level" placeholder="New Password" value="" style="width: 250px;" >
            <input type="password" id="confirm_password" class="input-block-level" placeholder="Confirm Password" style="width: 250px;" value="" onChange="checkPasswordMatch()" >
            <div style="font-family: b;" class="registrationFormAlert" id="divCheckPasswordMatch">
            </div> <br>
            <input type="button" id="submit" class="btn btn-large btn-primary" style="margin-left: 73px;" value="Submit" disabled />

        </form>
    </div>
</body>
<script src="js/bootbox.min.js"></script>
<script type="text/javascript" src="js/jqueryslidemenu.js"></script>
<script type="text/javascript" src="js/bootstrap-modal.js"></script>
</html>

