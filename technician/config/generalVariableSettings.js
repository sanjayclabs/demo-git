module.exports = {
    "production": {
        "port" : "3001",
        "iosGateway" : "gateway.push.apple.com",
        "pemFile" : "vizavoo_cust.pem",
        "androidKey" : "AIzaSyBZlNIHmUygCA5q42ru94URrnWs0sYqO_Y",
        "servicePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/service/",
        "profilePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/profile/",
        "stripeToken" : "sk_test_AXFBg4piX0Bbom8zKliOMLTn",
        "domainName" : "live.vizavoo.com"
    },
    "test": {
        "port" : "3001",
        "iosGateway" : "gateway.push.apple.com",
        "pemFile" : "vizavoo_cust.pem",
        "androidKey" : "AIzaSyBZlNIHmUygCA5q42ru94URrnWs0sYqO_Y",
        "servicePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/service/",
        "profilePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/profile/",
        "stripeToken" : "sk_test_AXFBg4piX0Bbom8zKliOMLTn",
        "domainName" : "api.vizavoo.com"
    },
    "development": {
        "port" : "3001",
        "iosGateway" : "gateway.push.apple.com",
        "pemFile" : "vizavoo_cust.pem",
        "androidKey" : "AIzaSyBZlNIHmUygCA5q42ru94URrnWs0sYqO_Y",
        "servicePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/service/",
        "profilePicBaseUrl" : "http://vizavoo.s3.amazonaws.com/profile/",
       "stripeToken" : "sk_test_AXFBg4piX0Bbom8zKliOMLTn",
        "domainName" : "api.vizavoo.com"

    }
};
