
exports.invalidAccessTokenError = function (res) {

    var errorMsg = {"error": 'Please login again, invalid access.',status:"false",flag:1};
    sendData(errorMsg,res);
};

exports.parameterMissingError = function (res) {

    var errorMsg = {"error": 'Some parameter missing, try again.',status:"false",flag:0};
    sendData(errorMsg,res);
};

exports.somethingWentWrongError = function (res) {

    var errorMsg = {"error": 'Something went wrong, try again.',status:"false",flag:0};
    sendData(errorMsg,res);
};


exports.sendSuccessData = function (data,res) {

    var successData = {status:"true",data:data};
    sendData(successData,res);
};

exports.sendError = function (error,res) {

    var successData = {status:"false",error:error,flag:0};
    sendData(successData,res);
};


exports.sendSuccessMessageWithData = function (data,message,res) {

    var successData = {status:"true",data:data,message:message};
    sendData(successData,res);
};

exports.sendErrorWithData = function (data,message,res) {

    var successData = {status:"false",data:data,message:message};
    sendData(successData,res);
};


exports.successStatusMsg = function (res) {

    var successMsg = {"status": "true"};
    sendData(successMsg,res);
};



function sendData(data,res)
{
    res.type('json');
    res.jsonp(data);
}


exports.sendData = function (data,res) {

    res.type('json');
    res.jsonp(data);
};

exports.fbRegisterError = function (res) {

    var errorMsg = {"error": 'Show Register Page',status:"false",flag:10};
    sendData(errorMsg,res);
};