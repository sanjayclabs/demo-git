var func = require('./commonfunction');
var sendResponse = require('./sendResponse');
var async = require('async');

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Add Credit Card of technician
 * INPUT : access_token, stripeToken, cardType
 * OUTPUT : success,error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.AddCreditCard = function (req, res) {

    var accessToken = req.body.access_token;
    var stripeToken = req.body.stripe_token;
    var cardType = req.body.card_type;         //  visa, master card

    console.log(req.body);
    var manValues = [accessToken, stripeToken, cardType];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            },
            function (userInfo, callback) {

                func.GetNumberOfCardsForTheUser(userInfo[0].technician_id, res, callback);

            }], function (err, userID, numberOfCards) {

            if (numberOfCards > 4) {
                var error = 'Can\'t add more than five cards.';
                sendResponse.sendError(error, res);
            }
            else {
                var defaultStatus = 1;
                if (numberOfCards) {
                    defaultStatus = 0;
                }
                AddCreditCardFunction(userID, stripeToken, defaultStatus, cardType, res);
            }
        }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * show Credit Cards of a tech
 * INPUT : access_token
 * OUTPUT : success,error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.ShowCreditCard = function (req, res) {

    var accessToken = req.body.access_token;

    console.log(req.body);
    var manValues = [accessToken];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var userID = userInfo[0].technician_id;


            var sql = "SELECT `card_id`, `technician_id`, `last_4`, `card_type`, `default_status` FROM `technician_cards` WHERE `technician_id`=?";
            connection.query(sql, [userID], function (err, response) {

                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var data = {"cards": response};
                    sendResponse.sendSuccessData(data, res);
                    //sendResponse.sendSuccessData(response, res);
                }
            })
        }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Adding credit card function using stripe
 * INPUT : userId, stripeToken, default status, card type
 * OUTPUT : success,error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function AddCreditCardFunction(userID, stripeToken, defaultStatus, cardType, res) {

    var stripe1 = require("stripe")(genVarSettings.stripeToken);

    stripe1.customers.create({
        card: stripeToken
    }, function (err, customer) {

        if (err) {

            var error = 'Invalid stripe token.';
            sendResponse.sendError(error, res);
        }
        else {
            var id = customer.id;
            var last_4 = customer.cards.data[0].last4;

            var sql = "INSERT INTO `technician_cards`(`technician_id`, `customer_id`, `last_4`, `default_status`, `card_type`) VALUES (?,?,?,?,?)";
            connection.query(sql, [userID, id, last_4, defaultStatus, cardType], function (err, result) {

                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var sql = "SELECT `card_id`, `technician_id`, `last_4`, `card_type`, `default_status` FROM `technician_cards` WHERE `technician_id`=?";
                    connection.query(sql, [userID], function (err, responseCardDetails) {
                        var data = {"message": "Card added Successfully", "cards": responseCardDetails};
                        sendResponse.sendSuccessData(data, res);
                    });
                }

            })
        }
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * delete Credit Card
 * INPUT : access_token, cardId
 * OUTPUT : success,error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.DeleteCreditCard = function (req, res) {

    var accessToken = req.body.access_token;
    var cardId = req.body.card_id;

    var manValues = [accessToken, cardId];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {
            var userId = userInfo[0].technician_id;
            var sql = "SELECT `default_status` FROM `technician_cards` WHERE `card_id`=? LIMIT 1";
            connection.query(sql, [cardId], function (err, responseCard) {
                if(responseCard.length == 0){
                    var sql = "SELECT `card_id`, `technician_id`, `last_4`, `card_type`, `default_status` FROM `technician_cards` WHERE `technician_id`=?";
                    connection.query(sql, [userId], function (err, responseCardDetails) {

                        var data = {"message": "Card Deleted Successfully.", "cards": responseCardDetails};
                        sendResponse.sendSuccessData(data, res);
                    });
                }
                else {


                    var updateFlag = 0;
                    if (responseCard[0].default_status == 1) {
                        updateFlag = 1;
                    }
                    var sql = "DELETE FROM `technician_cards` WHERE `card_id`=? LIMIT 1";
                    connection.query(sql, [cardId], function (err, responseDelete) {
                        if (err) {
                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            if (updateFlag == 1) {
                                var sql = "UPDATE `technician_cards` SET `default_status`=? WHERE `technician_id`=? LIMIT 1";
                                connection.query(sql, [1, userId], function (err, responseUpdate) {
                                    var sql = "SELECT `card_id`, `customer_id`, `last_4`, `card_type`, `default_status` FROM `technician_cards` WHERE `technician_id`=?";
                                    connection.query(sql, [userId], function (err, responseCardDetails) {
                                        var data = {
                                            "message": "Card Deleted Successfully.",
                                            "cards": responseCardDetails
                                        };
                                        sendResponse.sendSuccessData(data, res);
                                    });
                                });
                            }
                            else {
                                var sql = "SELECT `card_id`, `technician_id`, `last_4`, `card_type`, `default_status` FROM `technician_cards` WHERE `technician_id`=?";
                                connection.query(sql, [userId], function (err, responseCardDetails) {

                                    var data = {"message": "Card Deleted Successfully.", "cards": responseCardDetails};
                            sendResponse.sendSuccessData(data, res);
                        });
                    }

                        }

                    });
                }
            });

        }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * default Credit Card
 * INPUT : access_token, cardId
 * OUTPUT : success,error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.DefaultCard = function (req, res) {

    var accessToken = req.body.access_token;
    var cardId = req.body.card_id;

    var manValues = [accessToken, cardId];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {
            var userId = userInfo[0].technician_id;
            var sql = "UPDATE `technician_cards` SET `default_status`=? WHERE `technician_id`=? LIMIT 1";
            connection.query(sql, [0, userId], function (err, responseUpdate) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var sql = "UPDATE `technician_cards` SET `default_status`=? WHERE `card_id`=? LIMIT 1";
                    connection.query(sql, [1, cardId], function (err, responseDefaultUpdate) {
                        var data = {"message": "Default card changed successfully."};
                        sendResponse.sendSuccessData(data, res);
                    });
                }
            });
        }
    );
};