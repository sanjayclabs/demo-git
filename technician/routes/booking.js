var func = require('./commonfunction');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');
var generatePassword = require('password-generator');

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Fetch New Booking of a tech
 * INPUT : accessToken
 * OUTPUT : error,success
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.NewBookings = function (req, res) {

    var accessToken = req.body.access_token;

    console.log(req.body);
    var manValues = [accessToken];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var userID = userInfo[0].technician_id;
            getNewBookingsData(userID, res, function (response) {
                var data = {"new_bookings": response};
                sendResponse.sendSuccessData(data, res);
            });

        }
    );
};
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * get New Bookings data of a tech
 * INPUT : userID
 * OUTPUT : booking data
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getNewBookingsData(userID, res, callback) {
    var sql = "SELECT  a.`id`,b.`name`,a.`address`,a.`latitude`,a.`longitude`,a.`local_start_time`,a.`service_price`,a.`service_type`,a.`service_time`,b.`image`, c.`first_name`, c.`last_name`, c.`mobile` FROM `booking_timings` a, `service` b, `users` c, `booking_requests` d WHERE d.`technician_id`=? && d.`status` = ? && a.`id`=d.`booking_id` && a.`service_id`=b.`service_id` && a.`user_id`=c.`user_id`";
    connection.query(sql, [userID, 0], function (err, response) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var responseCount = response.length;
            if (responseCount > 0) {
                for (var i = 0; i < responseCount; i++) {
                    response[i].local_start_time = response[i].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    response[i].image = servicePicBaseUrl + response[i].image;
                }
            }
            return callback(response);

        }
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Accept/ Reject Booking
 * INPUT : accessToken,bookingId,isAccepted, reason
 * OUTPUT : error,success
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.AcceptBooking = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;
    var isAccepted = req.body.is_accepted;   // 1 for accepted, 0 for rejected
    var reason = req.body.reason;

    console.log(req.body);
    var manValues = [accessToken, bookingId, isAccepted];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var userID = userInfo[0].technician_id;
            var sql = "SELECT a.`id`,a.`user_id`,b.`service_price`,b.`card_id`, c.`credits` FROM `booking_requests` a, `booking_timings` b, `users` c WHERE a.`technician_id`=? " +
                "&& a.`status` = ? && a.`booking_id` = ? && a.`booking_id` = b.`id` && a.`user_id` = c.`user_id` LIMIT 1";
            connection.query(sql, [userID, 0, bookingId], function (err, responseBooking) {
                if (responseBooking.length > 0) {
                    if (isAccepted == 1) {
                        var credits = responseBooking[0].credits;
                        var dbAmount = responseBooking[0].service_price;

                        if (credits >= dbAmount) {
                            credits = credits - dbAmount;
                            dbAmount = 0;
                        }
                        else {
                            dbAmount = dbAmount - credits;
                            credits = 0;
                        }
                        dbAmount = dbAmount * 100;
                        if (dbAmount > 50) {
                            var sql = "SELECT `customer_id` FROM `card` WHERE `card_id`=? LIMIT 1";
                            connection.query(sql, [responseBooking[0].card_id], function (err, responseCard) {
                                if (responseCard.length == 1) {
                                    // Payment process
                                    var stripe = require("stripe")(genVarSettings.stripeToken);
                                    stripe.charges.create({
                                        amount: dbAmount,
                                        currency: "usd",
                                        customer: responseCard[0].customer_id, // obtained with Stripe.js
                                        description: "Charge for Vizavoo"
                                    }, function (err, charge) {
                                        if (err) {
                                            console.log("err");
                                            console.log(err);
                                            //var responseData = {"error": err.message};


                                            var sql = "UPDATE `booking_requests` SET `status`=? WHERE `booking_id`=?";
                                            connection.query(sql, [4, bookingId], function (err, requestsStatusUpdate) {
                                                var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                                                connection.query(sql, [7, bookingId], function (err, responseCancelUpdate) {
                                                    getNewBookingsData(userID, res, function (response) {

                                                        var message = "Booking cancelled due to some problem with payment.";
                                                        var data = {new_bookings: response};
                                                        sendResponse.sendErrorWithData(data, message, res);

                                                        //var error = {message:'Booking cancelled due to some problem with payment.',new_bookings: response};
                                                        //sendResponse.sendError(error, res);
                                                    });
                                                });
                                            });
                                            var message = 'Your booking cancelled due to some issues with payment.';
                                            sendPushToCustomer(responseBooking[0].user_id, message);

                                        }
                                        else {
                                            var transactionId = charge.id;
                                            var sql = "UPDATE `booking_requests` SET `status`=? WHERE `id`=? LIMIT 1";
                                            connection.query(sql, [1, responseBooking[0].id], function (err, responseAcceptUpdate) {
                                                var sql = "UPDATE `booking_requests` SET `status`=? WHERE `id`!=? && `booking_id` = ?";
                                                connection.query(sql, [3, responseBooking[0].id, bookingId], function (err, responseOthersUpdate) {
                                                    var sql = "UPDATE `booking_timings` SET `technician_id`=?,`status`=?, `transaction_id`=? WHERE `id`=? LIMIT 1";
                                                    connection.query(sql, [userID, 1, transactionId, bookingId], function (err, responseTechUpdate) {
                                                        getNewBookingsData(userID, res, function (response) {
                                                            var message = "Booking accepted successfully.";
                                                            var data = {new_bookings: response};
                                                            sendResponse.sendSuccessMessageWithData(data, message, res);
                                                            //sendResponse.sendSuccessData(data, res);

                                                        });
                                                        var sql = "UPDATE `users` SET `credits`=? WHERE `user_id`=? LIMIT 1";
                                                        connection.query(sql, [credits, responseBooking[0].user_id], function (err, responseCreditsUpdate) {

                                                        });

                                                        var message = 'Your booking request accepted.';
                                                        sendPushToCustomer(responseBooking[0].user_id, message);


                                                    });
                                                });
                                            });
                                        }
                                    });
                                }
                                else {


                                    var sql = "UPDATE `booking_requests` SET `status`=? WHERE `booking_id`=?";
                                    connection.query(sql, [4, bookingId], function (err, requestsStatusUpdate) {
                                        var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                                        connection.query(sql, [7, bookingId], function (err, responseCancelUpdate) {
                                            getNewBookingsData(userID, res, function (response) {

                                                //var error = {message:'Booking cancelled due to some problem with payment.',new_bookings: response};
                                                //sendResponse.sendError(error, res);
                                                var message = "Booking cancelled due to some problem with payment.";
                                                var data = {new_bookings: response};
                                                sendResponse.sendErrorWithData(data, message, res);
                                            });
                                        });
                                    });

                                    var message = 'Your booking cancelled due to some issues with payment.';
                                    sendPushToCustomer(responseBooking[0].user_id, message);
                                }
                            });
                        }

                    }

                    else {
                        var sql = "UPDATE `booking_requests` SET `status`=?, `reason`=? WHERE `id`=? limit 1";
                        connection.query(sql, [2, reason, responseBooking[0].id], function (err, responseRejectUpdate) {
                            getNewBookingsData(userID, res, function (response) {
                                //var data = {"message": "Booking rejected successfully.",new_bookings: response};
                                //sendResponse.sendSuccessData(data, res);

                                var message = "Booking rejected successfully.";
                                var data = {new_bookings: response};
                                sendResponse.sendSuccessMessageWithData(data, message, res);


                            });


                            var sql = "SELECT `id` FROM `booking_requests` WHERE `status` = ? && `booking_id` = ? LIMIT 1";
                            connection.query(sql, [0, bookingId], function (err, responseLeftBooking) {
                                if (responseLeftBooking.length == 0) {
                                    var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                                    connection.query(sql, [6, bookingId], function (err, responseStatusUpdate) {

                                    });
                                }

                            });
                        });
                    }
                }
                else {
                    getNewBookingsData(userID, res, function (response) {
                        var message = "Booking accepted by other.";
                        var data = {new_bookings: response};
                        sendResponse.sendErrorWithData(data, message, res);

                        //var error = {message:'Booking accepted by other.',new_bookings: response};
                        //sendResponse.sendError(error, res);
                    });

                }
            });


        }
    );
};
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * send Push To Customer(android/iOS)
 * INPUT : userId
 * OUTPUT : Check device type of user & call the respective sending push function accordingly
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function sendPushToCustomer(userId, message) {
    var sql = "SELECT `device_type`, `device_token` FROM `users` WHERE `user_id`= ? LIMIT 1";
    connection.query(sql, [userId], function (err, resultDevices) {

        //var resultTechsDevicesCount = resultTechsDevices.length;
        var androidDevices = [];
        var iosDevices = [];
        // for (var t = 0; t < resultTechsDevicesCount; t++) {
        if (resultDevices[0].device_type == 1) {
            androidDevices.push(resultDevices[0].device_token);
        }
        else {
            iosDevices.push(resultDevices[0].device_token);
        }
        //}
        //var message = 'Your request accepted.';
        func.sendApplePushNotification(iosDevices, message);
        func.sendAndroidPushNotification(androidDevices, message);

    });
}

/*
 * -----------------------------------------------------------------------------
 * prototype for repeating a string
 * INPUT : string and no. of times the string has to be repeated
 * OUTPUT : repeated string
 * -----------------------------------------------------------------------------
 */
String.prototype.repeat = function (num) {
    return new Array(num + 1).join(this);
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * out to service
 * INPUT : accessToken, bookingId
 * OUTPUT : out to service
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.OutToService = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;

    console.log(req.body);
    var manValues = [accessToken, bookingId];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var sql = "SELECT `user_id` FROM `booking_timings` WHERE `id`= ? LIMIT 1";
            connection.query(sql, [bookingId], function (err, resultBooking) {
                if (resultBooking.length > 0) {
                    var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                    connection.query(sql, [2, bookingId], function (err, responseUpdate) {
                        if (err) {
                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            var data = {"message": "Out for service."};
                            sendResponse.sendSuccessData(data, res);

                            var sql = "SELECT `device_type`, `device_token` FROM `users` WHERE `user_id`= ? LIMIT 1";
                            connection.query(sql, [resultBooking[0].user_id], function (err, resultDevices) {

                                //var resultTechsDevicesCount = resultTechsDevices.length;
                                var androidDevices = [];
                                var iosDevices = [];
                                // for (var t = 0; t < resultTechsDevicesCount; t++) {
                                if (resultDevices[0].device_type == 1) {
                                    androidDevices.push(resultDevices[0].device_token);
                                }
                                else {
                                    iosDevices.push(resultDevices[0].device_token);
                                }
                                //}
                                var message = 'Your service provider is on his way.';
                                func.sendApplePushNotification(iosDevices, message);
                                func.sendAndroidPushNotification(androidDevices, message);

                            });
                        }
                    });
                }
                else {
                    var error = 'Booking doesn\'t exist now.';
                    sendResponse.sendError(error, res);
                }
            });

        }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Reached
 * INPUT : accessToken, bookingId
 * OUTPUT : reached at site
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.Reached = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;

    console.log(req.body);
    var manValues = [accessToken, bookingId];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {

            var sql = "SELECT `user_id` FROM `booking_timings` WHERE `id`= ? LIMIT 1";
            connection.query(sql, [bookingId], function (err, resultBooking) {
                if (resultBooking.length > 0) {
                    var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
                    connection.query(sql, [3, bookingId], function (err, responseUpdate) {
                        if (err) {
                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            var data = {"message": "Reached."};
                            sendResponse.sendSuccessData(data, res);

                            var sql = "SELECT `device_type`, `device_token` FROM `users` WHERE `user_id`= ? LIMIT 1";
                            connection.query(sql, [resultBooking[0].user_id], function (err, resultDevices) {

                                //var resultTechsDevicesCount = resultTechsDevices.length;
                                var androidDevices = [];
                                var iosDevices = [];
                                // for (var t = 0; t < resultTechsDevicesCount; t++) {
                                if (resultDevices[0].device_type == 1) {
                                    androidDevices.push(resultDevices[0].device_token);
                                }
                                else {
                                    iosDevices.push(resultDevices[0].device_token);
                                }
                                //}
                                var message = 'Your service provider reached.';
                                func.sendApplePushNotification(iosDevices, message);
                                func.sendAndroidPushNotification(androidDevices, message);

                            });
                        }
                    });
                }
                else {
                    var error = 'Booking doesn\'t exist now.';
                    sendResponse.sendError(error, res);
                }
            });

        }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Start service
 * INPUT : accessToken, bookingId
 * OUTPUT : bookings started
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.StartService = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;

    console.log(req.body);
    var manValues = [accessToken, bookingId];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {


            var sql = "UPDATE `booking_timings` SET `status`=? WHERE `id`=? LIMIT 1";
            connection.query(sql, [4, bookingId], function (err, responseUpdate) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var data = {"message": "Booking started successfully."};
                    sendResponse.sendSuccessData(data, res);
                }
            });


        }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * End service
 * INPUT : accessToken, bookingId
 * OUTPUT : bookings ended
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.EndService = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;
    var serviceTime = req.body.service_time;

    console.log(req.body);
    var manValues = [accessToken, bookingId, serviceTime];

    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function (callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

            }], function (err, userInfo) {
        var sql = "SELECT `offset` FROM `booking_timings` WHERE `id`= ? LIMIT 1";
        connection.query(sql, [bookingId], function (err, resultBooking) {
            if(resultBooking.length > 0) {


                var curTime = new Date();
                var localTime = new Date();
                var offset = resultBooking[0].offset;
                localTime.setMinutes(localTime.getMinutes() + offset);
                var sql = "UPDATE `booking_timings` SET `status`=?,`actual_service_time`=?,`end_time`=?,`local_end_time`=? WHERE `id`=? LIMIT 1";
                connection.query(sql, [5, serviceTime, curTime, localTime, bookingId], function (err, responseUpdate) {
                    console.log(err)
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var data = {"message": "Booking ended successfully."};
                        sendResponse.sendSuccessData(data, res);
                    }
                });
            }
            else{
                var error = 'Booking doesn\'t exist now.';
                sendResponse.sendError(error, res);
            }
        });
        }
    );
};
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get ongoing schedule data
 * INPUT : user_id
 * OUTPUT : booking data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getOngoingSchedule(res, userID, callback) {
    //var curDate = new Date();
    var sql = "SELECT  a.`id`,b.`name`,a.`service_type` FROM `booking_timings` a, `service` b WHERE a.`user_id`=? && a.`status`=? && a.`service_id`=b.`service_id` LIMIT 1";
    connection.query(sql, [userID, 2], function (err, responseOngoing) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {

            callback(null, responseOngoing);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get past schedule data
 * INPUT : user_id
 * OUTPUT : booking data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getPastSchedules(res, userID, callback) {
    //var curDate = new Date();
    var sql = "SELECT  a.`id`,b.`name`,a.`address`,a.`start_time`,a.`service_price`,a.`service_type`,b.`image`,a.`status` FROM `booking_timings` a, `service` b WHERE a.`user_id`=? && a.`status`=? && a.`service_id`=b.`service_id`";
    connection.query(sql, [userID, 3], function (err, responsePast) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var responsePastCount = responsePast.length;
            for (var i = 0; i < responsePastCount; i++) {
                responsePast[i].start_time = responsePast[i].start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                responsePast[i].image = servicePicBaseUrl + responsePast[i].image;
            }

            callback(null, responsePast);
        }
    });
}
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get upcoming schedule data
 * INPUT : user_id
 * OUTPUT : booking data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getUpcomingSchedules(res, userID, callback) {
    var curDate = new Date();
    var sql = "SELECT  a.`id`,b.`name`,a.`address`,a.`start_time`,a.`service_price`,a.`service_type`,b.`image`,a.`status` FROM `booking_timings` a, `service` b WHERE a.`user_id`=? && a.`start_time`>? && a.`service_id`=b.`service_id` GROUP BY a.`booking_id`";
    connection.query(sql, [userID, curDate], function (err, responseUpcoming) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var responseUpcomingCount = responseUpcoming.length;
            for (var i = 0; i < responseUpcomingCount; i++) {
                responseUpcoming[i].start_time = responseUpcoming[i].start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                responseUpcoming[i].image = servicePicBaseUrl + responseUpcoming[i].image;
            }

            callback(null, responseUpcoming);

        }
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Give rating to customer
 * INPUT : accessToken, bookingId,rating
 * OUTPUT : rating given to technician
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.GiveRating = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;
    var rating = req.body.rating;

    var manValues = [accessToken, bookingId, rating];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {

            var extraDataNeeded = [];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function (err, userInfo) {
        var userID = userInfo[0].technician_id;
        var sql = "SELECT `user_id` FROM `booking_timings` WHERE `id`=? LIMIT 1";
        connection.query(sql, [bookingId], function (err, responseUser) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                var sql = "UPDATE `booking_timings` SET `user_rating`=? WHERE `id` = ? LIMIT 1";
                connection.query(sql, [rating, bookingId], function (err, responseBookingRating) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var sql = "UPDATE `users` SET `rating_count`=`rating_count`+?, `total_rating`=`total_rating`+? WHERE `user_id` = ? LIMIT 1";
                        connection.query(sql, [1, rating, responseUser[0].user_id], function (err, responseUserRating) {
                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                func.HomeScreenData(userID, function (services) {

                                    //var data = {"message": "Rating done successfully.","home_data":services};
                                    //sendResponse.sendSuccessData(data, res);

                                    var message = "Rating done successfully.";
                                    var data = {home_data: services};
                                    sendResponse.sendSuccessMessageWithData(data, message, res);
                                });

                            }

                        });
                    }
                });

            }

        });
    });
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * fetch upcoming bookings of technician
 * INPUT : accessToken
 * OUTPUT : bookings data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.UpcomingBookings = function (req, res) {

    var accessToken = req.body.access_token;
    var curLocalDate = req.body.current_date;
    console.log(req.body);
    var manValues = [accessToken, curLocalDate];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {

            var extraDataNeeded = [];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function (err, userInfo) {
        var technicianId = userInfo[0].technician_id;
        var curDate = new Date();
        var sql = "SELECT b.`id`,a.`name`,b.`address`,b.`local_start_time`, DATE(b.`local_start_time`) as local_date, b.`service_price`,b.`status`, a.`image` " +
            "FROM `booking_timings` b, `service` a WHERE b.`technician_id`=? && b.`start_time`> ? && (b.`status` = ? || b.`status`=?) && b.`service_id` = a.`service_id`";
        connection.query(sql, [technicianId, curDate, 1, 7], function (err, responseBookings) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                var responseBookingsCount = responseBookings.length;

                var tomorrow = new Date(curLocalDate);
                var bookings = [];

                for (var j = 0; j < 15; j++) {
                    var todayDate = new Date(tomorrow);
                    todayDate = todayDate.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    todayDate = todayDate.split(" ");
                    todayDate = todayDate[0];
                    var bookingData = [];
                    var totalPayment = 0;
                    var totalService = 0;
                    //console.log('tomorrow' + tomorrow)
                    for (var i = 0; i < responseBookingsCount; i++) {

                        //console.log('dbDate' + responseBookings[i].local_date)
                        if (new Date(responseBookings[i].local_date).getTime() == new Date(tomorrow).getTime()) {
                            responseBookings[i].local_start_time = responseBookings[i].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                            responseBookings[i].image = servicePicBaseUrl + responseBookings[i].image;
                            totalPayment += responseBookings[i].service_price;
                            totalService += 1;
                            bookingData.push(responseBookings[i]);
                            //delete responseBookings[i].service_price;
                            // delete responseBookings[i].local_date;
                        }

                    }
                    bookings.push({
                        "date": todayDate,
                        "booking_data": bookingData,
                        "total_payment": totalPayment,
                        "total_service": totalService
                    });

                    tomorrow.setDate(tomorrow.getDate() + 1);
                }

                var data = {"bookings": bookings};
                sendResponse.sendSuccessData(data, res);

            }
        });

    });
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * fetch details of one specific booking
 * INPUT : accessToken, bookingId
 * OUTPUT : bookings data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.BookingDetails = function (req, res) {

    var accessToken = req.body.access_token;
    var bookingId = req.body.booking_id;

    var manValues = [accessToken, bookingId];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {

            var extraDataNeeded = [];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function (err, userInfo) {
        var sql = "SELECT  a.`id`,b.`name`,a.`address`,a.`latitude`,a.`longitude`,a.`local_start_time`,a.`service_price`,a.`service_type`,a.`service_time`,b.`image`, c.`first_name`, c.`last_name`, c.`mobile` FROM `booking_timings` a, `service` b, `users` c WHERE a.`id` =? LIMIT 1";
        connection.query(sql, [bookingId], function (err, responseSchedules) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (responseSchedules.length > 0) {
                    responseSchedules[0].local_start_time = responseSchedules[0].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    responseSchedules[0].image = servicePicBaseUrl + responseSchedules[0].image;

                    var data = {"booking_details": responseSchedules};
                    sendResponse.sendSuccessData(data, res);
                }
                else {
                    var error = "Booking doesn't exist.";
                    sendResponse.sendError(error, res);
                }
            }

        });

    });
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Send reminder push to the techs before 2 hrs of service time
 * INPUT : Cron runs every half an hour
 * OUTPUT : push send
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.TwoHoursBeforeServiceCron = function (req, res) {

    var startDate = new Date();

    var endDate = new Date();

    var startMinutes = 118;
    startDate.setMinutes(startDate.getMinutes() + startMinutes);
    startDate = func.getDateTime(startDate, 0);

    var endMinutes = 122;
    endDate.setMinutes(endDate.getMinutes() + endMinutes);

    endDate = func.getDateTime(endDate, 0);
    console.log('startDate' + startDate);
    console.log('endDate' + endDate);

    var sql = "SELECT  a.`id`,b.`device_type`,b.`device_token` FROM `booking_timings` a, `technicians` b WHERE a.`status` = ? && (a.`start_time` BETWEEN '" + startDate + "' AND '" + endDate + "') && a.technician_id = b.technician_id";
    connection.query(sql, [1], function (err, responseSchedules) {
        console.log(err);
        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            console.log(responseSchedules);
            var responseSchedulesCount = responseSchedules.length;
            var androidDevices = [];
            var iosDevices = [];
            for (var t = 0; t < responseSchedulesCount; t++) {
                if (responseSchedules[t].device_type == 1) {
                    androidDevices.push(responseSchedules[t].device_token);
                }
                else {
                    iosDevices.push(responseSchedules[t].device_token);
                }
            }
            var message = 'You have a service coming up in two hours.';
            func.sendApplePushNotification(iosDevices, message);
            func.sendAndroidPushNotification(androidDevices, message);
            var data = {"message": "Done."};
            sendResponse.sendSuccessData(data, res);
        }
    });
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get state of a technician
 * INPUT : accessToken
 * OUTPUT : state of user fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.InitialCall = function (req, res) {

    var accessToken = req.body.access_token;
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
console.log(req.body)
    var manValues = [accessToken,deviceType,appVersion];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {

            var extraDataNeeded = [];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);
        },
        //}], function (err, userInfo) {
            function(userInfo, callback) {
            func.checkAppVersion(deviceType, appVersion, res,function(err, updatePopup, critical) {
                callback(null, updatePopup, critical, userInfo);
            });

        }], function(err, updatePopup, critical, userInfo) {
        var techId = userInfo[0].technician_id;
        var loginTime = new Date();
        func.updateUserParams(deviceToken, deviceType, loginTime, appVersion, techId);

        var sql1 = "SELECT  a.`id`, a.`status`,b.`name`,a.`address`,a.`latitude`,a.`longitude`,a.`local_start_time`,a.`service_price`,a.`service_type`,a.`service_time`," +
            "b.`image`, c.`first_name`, c.`last_name`, c.`mobile`,a.`user_rating`, a.`last_updated` FROM `booking_timings` a, `service` b, `users` c WHERE " +
            " a.`service_id`=b.`service_id` && a.`technician_id` = ? && a.`user_id` = c.`user_id` && a.`status` NOT IN(0,1,6,7) LIMIT 1";
        connection.query(sql1, [techId], function (err, responseState) {

            console.log(err);
            console.log(responseState);

            var responseStateCount = responseState.length;
            if (responseStateCount == 0) {
                func.GetCardsDataOfTech(techId, function(cards) {
                    func.HomeScreenData(techId, function (services) {
                        var data = {update_popup: updatePopup, critical: critical,home_data:services,cards:cards,flag:0};
                        //data.home_data = services;
                        //data.cards = cards;
                        // data.flag = 0;            // home screen
                        sendResponse.sendSuccessData(data, res);
                    });
                });
            }
            else {
                responseState[0].local_start_time = responseState[0].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                responseState[0].image = servicePicBaseUrl + responseState[0].image;

                if (responseState[0].status == 2) {   // on the way


                    //var data = {"booking_details": responseState, "flag": 1};

                    func.GetCardsDataOfTech(techId, function(cards) {
                        func.HomeScreenData(techId, function (services) {
                            services.upcoming = responseState;
                            var data = {update_popup: updatePopup, critical: critical,home_data:services,cards:cards,flag:1};
                            //data.home_data = services;
                            //data['home_data'].upcoming = responseState;
                            //data.cards = cards;
                            //data.flag = 1;
                            sendResponse.sendSuccessData(data, res);
                        });
                    });

                    //sendResponse.sendSuccessData(data, res);


                }
                else if (responseState[0].status == 3) {   // reached


                    //var data = {"booking_details": responseState, "flag": 2};
                    func.GetCardsDataOfTech(techId, function(cards) {
                        func.HomeScreenData(techId, function (services) {
                            //data.home_data = services;
                            //data['home_data'].upcoming = responseState;
                            //data.cards = cards;
                            //data.flag = 2;
                            services.upcoming = responseState;
                            var data = {update_popup: updatePopup, critical: critical,home_data:services,cards:cards,flag:2};
                            sendResponse.sendSuccessData(data, res);
                        });
                    });

                    //sendResponse.sendSuccessData(data, res);


                }

                else if (responseState[0].status == 4) {      // service started
                    var dbDate = responseState[0].last_updated.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    var startDate = new Date(dbDate);
                    var timeDiff = func.getTimeDifference(startDate, 2);
                    console.log(timeDiff);
                    responseState[0].last_updated = timeDiff;
                    //var data = {"booking_details": responseState, "flag": 3};

                    //sendResponse.sendSuccessData(data, res);
                    func.GetCardsDataOfTech(techId, function(cards) {
                        func.HomeScreenData(techId, function (services) {
                            services.upcoming = responseState;
                            var data = {update_popup: updatePopup, critical: critical,home_data:services,cards:cards,flag:3};
                            //data.home_data = services;
                            //data.home_data.upcoming = responseState;
                            //data.cards = cards;
                            //data.flag = 3;

                            sendResponse.sendSuccessData(data, res);
                        });
                    });


                }
                else if (responseState[0].status == 5) {      // service ended
                    if (responseState[0].user_rating == 0) {      // show rating screen


                        //var data = {"booking_details": responseState, "flag": 4};
                        //
                        //sendResponse.sendSuccessData(data, res);

                        func.GetCardsDataOfTech(techId, function(cards) {
                            func.HomeScreenData(techId, function (services) {
                                services.upcoming = responseState;
                                var data = {update_popup: updatePopup, critical: critical,home_data:services,cards:cards,flag:4};
                                //data.home_data = services;
                                //data['home_data'].upcoming = responseState;
                                //data.cards = cards;
                                //data.flag = 4;

                                sendResponse.sendSuccessData(data, res);
                            });
                        });

                    }
                    else {                                       // home screen
                        func.GetCardsDataOfTech(techId, function(cards) {
                            func.HomeScreenData(techId, function (services) {

                                var data = {update_popup: updatePopup, critical: critical,home_data:services,cards:cards,flag:0};
                                //data.home_data = services;
                                //data.cards = cards;
                                //data.flag = 0;            // home screen
                                sendResponse.sendSuccessData(data, res);
                            });
                        });
                    }


                }
            }
        });
    });
};