var func = require('./commonfunction');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');
var generatePassword = require('password-generator');

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Register From Email
 * INPUT : firstName, lastName, email, mobile, password, deviceType, deviceToken, appVersion
 * OUTPUT : user information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.RegisterFromEmail = function(req, res) {

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var mobile = req.body.mobile;
    var password = req.body.password;
    var qualifications = req.body.qualifications;
    var techType = req.body.tech_type;
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
    var city = req.body.city;

    console.log(req.body);

    var manValues = [firstName, email, mobile, password, deviceType, appVersion, qualifications, techType];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {
            checkEmailAvailability(res, email, callback);
        },
        function(callback) {

            func.getImageNameAfterUpload(req.files.profile_pic, "profile", function(imageName) {
                callback(imageName);
            });
        }], function(imageName) {

        var loginTime = new Date();
        var accessToken = func.encrypt(email + loginTime);
        var encryptPassword = md5(password);
        imageName = profilePicBaseUrl + imageName;
        var sql = "INSERT INTO `technicians`(`access_token`, `first_name`, `last_name`, `email`, `mobile`, `password`, `fb_id`, `registration_type`, `device_type`, `device_token`, `updated_at`,`app_version`,`image`,`qualification`,`technician_type`,`city`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connection.query(sql, [accessToken, firstName, lastName, email, mobile, encryptPassword, 0, 1, deviceType, deviceToken, loginTime, appVersion, imageName, qualifications, techType,city], function(err, response) {

            if (err) {
                console.log(err)
                sendResponse.somethingWentWrongError(res);
            }
            else {

                var data = {"message": "Thanks for registration. Please wait till Admin approves your request."};
                sendResponse.sendSuccessData(data, res);

                fillAvalaibilityCalendar(response.insertId);

            }
        });
    }
    );
};


function fillAvalaibilityCalendar(techId) {

    var date = new Date();
    date = func.getDateOnly(date);

    //Setting the interval of application for example 60,30,15 minutes
    var interval = 30;
    var offset = (60 / interval);
    //Setting the default availabilty time for technician{Here the technician is available from 8:00 to 18:00}
    var availbiltyStartingHour = 0;
    var availabiltyEndingHour = 24;
    var offset = (60 / interval);
    var totalColunms = (24 * offset)
    var hoursArray = [];
    var availabilityInformation = [];
    var boolarray = new Array();
    var j = 1;
    for (var i = 0; i < totalColunms; i++) {
        if (i >= (availbiltyStartingHour * offset) && i <= (availabiltyEndingHour * offset)) {
            var intervalString = 0;
        }
        else {
            var intervalString = 1;
        }
        boolarray.push(intervalString);
        if (j % offset == 0) {
            hoursArray.push(boolarray);
            var boolarray = new Array();
        }
        ++j;
    }
    var nextMonth = new Date(date);
    //nextMonth.setMonth(nextMonth.getMonth() + 1)
    
    nextMonth.setDate(nextMonth.getDate() + 14);
    
    for (var d = new Date(date); d <= nextMonth; d.setDate(d.getDate() + 1)) {
        var filteredDate = (d.toISOString().replace(/T/, ' ').replace(/\..+/, '')).split(' ');
        availabilityInformation.push({schedule_date: filteredDate[0], hours: hoursArray});
    }

    var jsonData = availabilityInformation;
    var availabiltyLength = jsonData.length;
    var finalArray = [];

    for (var i = 0; i < availabiltyLength; i++) {
        var count = 0;
        var updatedData = '(' + '"' + +techId + '","' + jsonData[i]['schedule_date'] + '", ';
        var dynamicColumns = '';
        var totalHours = jsonData[i].hours.length;
        for (var j = 0; j < totalHours; j++) {
            var innerHourArrayLength = jsonData[i].hours[j].length;
            for (var k = 0; k < innerHourArrayLength; k++) {
                var now = new Date();
                now.setHours(0 + parseInt(count / innerHourArrayLength));
                now.setMinutes((count % innerHourArrayLength) * 30);
                var dynamicIntervals = pad(now.getHours(), 2) + ':' + pad(now.getMinutes(), 2);
                count++;
                if (jsonData[i].hours[j][k] == 1) {
                    var booleanValue = 1
                }
                else {
                    var booleanValue = 0;
                }
                dynamicColumns += '`' + dynamicIntervals + '`,';
                updatedData += booleanValue + ',';
            }
        }
        updatedData = updatedData.substring(0, updatedData.length - 1);
        updatedData += ')';
        finalArray.push(updatedData);
    }
    

    dynamicColumns = dynamicColumns.substring(0, dynamicColumns.length - 1);
    //console.log(finalArray);
    var sql = "INSERT INTO `availability`(`technician_id`,`schedule_date`," + dynamicColumns + ") VALUES "+ finalArray;
    connection.query(sql, function(err, resultInsert)
    {
        if (err)
            console.log(err)
    });

}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * EmailLogin
 * INPUT : email, password, deviceType, deviceToken, appVersion
 * OUTPUT : user information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.EmailLogin = function(req, res) {

    var email = req.body.email;
    var password = req.body.password;
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    console.log(req.body);

    var manValues = [email, password, deviceType, appVersion];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {

            func.checkAppVersion(deviceType, appVersion,res, callback);

        },
        function(updatePopup, critical, callback) {

            checkEmailPasswordAndUserType(res, updatePopup, critical, email, password, callback);

        }], function(err, updatePopup, critical, response) {

        var userID = response[0].technician_id;
        var accessToken = response[0].access_token;
        var city = response[0].city;
        var techType = response[0].technician_type;
        var loginTime = new Date();


        func.updateUserParams(deviceToken, deviceType, loginTime, appVersion, userID);

        var data = {access_token: accessToken, update_popup: updatePopup, critical: critical, first_name: response[0].first_name, last_name: response[0].last_name, mobile: response[0].mobile, image: response[0].image, user_id: response[0].technician_id, email: email, city:city, technician_type:techType};
            func.GetCardsDataOfTech(userID, function(cards) {
            func.HomeScreenData(userID, function (services) {
                data.home_data = services;
                data.cards = cards;
                sendResponse.sendSuccessData(data, res);
            });
        });
    }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * check Email Availability
 * INPUT : email
 * OUTPUT : email available or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getLoginInformation(userID, callback) {


    var sql = "SELECT `technician_id`, `access_token`, `first_name`, `last_name`, `email`, `image`, `mobile`, `fb_id`, `registration_type` FROM `technicians` WHERE `technician_id`=? limit 1";
    connection.query(sql, [userID], function(err, response) {


    });

}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Register From Fb
 * INPUT : firstName, lastName, email, mobile, password, deviceType, deviceToken, appVersion
 * OUTPUT : user information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.FbRegister = function(req, res) {

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var mobile = req.body.mobile;
    var password = req.body.password;
    var qualifications = req.body.qualifications;
    var techType = req.body.tech_type;
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
    var fbId = req.body.fb_id;
    var city = req.body.city;

    console.log(req.body);

    var manValues = [firstName, email, mobile, password, deviceType, appVersion, qualifications, techType, fbId];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {
            checkEmailAvailability(res, email, callback);
        },
        function(callback) {

              if ((req.files) && (req.files.profile_pic) && (req.files.profile_pic.name)) {

                func.getImageNameAfterUpload(req.files.profile_pic, "profile", function(imageName) {
                    callback(profilePicBaseUrl+imageName);
                });
            }
            else {
                callback("http://graph.facebook.com/" + fbId + "/picture?width=160&height=160");
            }
        }], function(imageName) {

        var loginTime = new Date();
        var accessToken = func.encrypt(email + loginTime);
        var encryptPassword = md5(password);
//        if (imageName == 'logo.jpeg')
//        {
//            imageName = "http://graph.facebook.com/" + fbId + "/picture?width=160&height=160";
//        }
//        else
//        {
//            imageName = profilePicBaseUrl + imageName;
//        }
        var sql = "INSERT INTO `technicians`(`access_token`, `first_name`, `last_name`, `email`, `mobile`, `password`, `fb_id`, `registration_type`, `device_type`, `device_token`, `updated_at`,`app_version`,`image`,`qualification`,`technician_type`,`city`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connection.query(sql, [accessToken, firstName, lastName, email, mobile, encryptPassword, fbId, 2, deviceType, deviceToken, loginTime, appVersion, imageName, qualifications, techType, city], function(err, response) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {

                var data = {"message": "Thanks for registration. Please wait till Admin approves your request."}
                sendResponse.sendSuccessData(data, res);
                fillAvalaibilityCalendar(response.insertId);
            }
        });
    }
    );
};

//function checkIfImageIsAvailableOrNot(file,callback){  
//    if ((req.files) && (req.files.profile_pic) && (req.files.profile_pic.name)) {
//        func.getImageNameAfterUpload(file.profile_pic, "profile", function(imageName) {
//        return callback(imageName);
//    });
//    }
//    else {
//        return callback("logo.jpeg")
//    }
//    
//}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * forgot Password
 * INPUT : email
 * OUTPUT : success,error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.ForgotPassword = function(req, res) {

    var email = req.body.email;
    var manValues = [email];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);

        }, function(callback) {

            getUserIDFromEmail(res, email, callback);

        }], function(err, userID) {


        var link = generatePassword(20, false);
        var date = new Date();
        link = md5(date + link);

        var sql = "UPDATE `technicians` SET `link_one_time_password`=?,`forgot_password_set`=? WHERE `technician_id`=? LIMIT 1";
        connection.query(sql, [link, 1, userID], function(err, response1) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                forgotPasswordRequest(email, link, function(result) {

                    if (result == 0) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var data = {"message": "Check your email to change your password."}
                        sendResponse.sendSuccessData(data, res);
                    }
                });
            }
        });
    }
    );
};




/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Fb login
 * INPUT : fbId,fbName,fbUsername,fbAccessToken,deviceToken, deviceName, appVersion
 * OUTPUT : Register user and return ACCESS TOKEN
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.FbLogin = function(req, res) {


    var email = req.body.email;
    var fbId = req.body.fb_id;
    var fbAccessToken = req.body.fb_access_token
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    var manValues = [email, fbId, fbAccessToken, deviceType, appVersion];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {
            func.checkAppVersion(deviceType, appVersion,res, callback);
        },
        function(updatePopup, critical, callback) {
            checkFbUser(res, updatePopup, critical, fbId, fbAccessToken, callback);
        }], function(err, updatePopup, critical, results) {

        var loginTime = new Date();



        if (results.length) {

            //login
            var userID = results[0].technician_id;
            var accessToken = results[0].access_token;
            var city = results[0].city;
            var techType = results[0].technician_type;

         if(results[0].is_block == 1){
                var data = "Your account is blocked. Contact Admin for further details.";
                sendResponse.sendError(data, res);
            }
            else if(results[0].is_block == 2){
                var data = "Your account is deleted. Contact Admin for further details.";
                sendResponse.sendError(data, res);
            }
            else if (results[0].driver_verified == 0)
            {
                var data = {"message": "Thanks for registration. Please wait till Admin approves your request."};
                sendResponse.sendSuccessData(data, res);

            }

            else
            {
                func.updateUserParams(deviceToken, deviceType, loginTime, appVersion, userID);
                func.GetCardsDataOfTech(userID, function(cards) {
                    func.HomeScreenData(userID, function (services) {
                        var data = {
                            "new": 0,
                            "access_token": accessToken,
                            update_popup: updatePopup,
                            critical: critical,
                            first_name: results[0].first_name,
                            last_name: results[0].last_name,
                            mobile: results[0].mobile,
                            image: results[0].image,
                            user_id: userID,
                            email: results[0].email,
                            city: city,
                            technician_type: techType
                        };

                        data.home_data = services;
data.cards = cards;

                        sendResponse.sendSuccessData(data, res);
                    });
                });
            }

        }
        else {

            //register

            checkEmailForFbRegister(email, function(userResult) {

                if (userResult.length) {

                    var userID = userResult[0].technician_id;
                    var accessToken = userResult[0].access_token;
                    var firstName = userResult[0].first_name;
                    var lastName = userResult[0].last_name;
                    var mobile = userResult[0].mobile;
                    var imageName = userResult[0].image;
                    var city = userResult[0].city;
                    var techType = userResult[0].technician_type;

                    if(userResult[0].is_block == 1){
                        var data = "Your account is blocked. Contact Admin for further details.";
                        sendResponse.sendError(data, res);
                    }
                    else if(userResult[0].is_block == 2){
                        var data =  "Your account is deleted. Contact Admin for further details.";
                        sendResponse.sendError(data, res);
                    }
                    else {
                        var sql = "UPDATE `technicians` SET `fb_id`=? ,`registration_type`=? WHERE `technician_id`=? limit 1";
                        connection.query(sql, [fbId, 3, userID], function (err, updateResult) {

                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {

                                if (userResult[0].driver_verified == 0) {
                                    var data = {"message": "Thanks for registration. Please wait till Admin approves your request."};
                                    sendResponse.sendSuccessData(data, res);
                                }
                                else {
                                    func.updateUserParams(deviceToken, deviceType, loginTime, appVersion, userID);


                                    var data = {
                                        "new": 0,
                                        "access_token": accessToken,
                                        update_popup: updatePopup,
                                        critical: critical,
                                        first_name: firstName,
                                        last_name: lastName,
                                        mobile: mobile,
                                        image: imageName,
                                        user_id: userID,
                                        email: email,
                                        city: city,
                                        technician_type: techType
                                    };
                                    func.GetCardsDataOfTech(userID, function(cards) {
                                        func.HomeScreenData(userID, function (services) {
                                            data.home_data = services;
data.cards = cards;
                                            sendResponse.sendSuccessData(data, res);
                                        });
                                    });
                                }
                            }
                        });
                    }
                }
                else {

                    sendResponse.fbRegisterError(res);
                }
            });
        }

    }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * user Logout
 * INPUT : email
 * OUTPUT : success,error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.UserLogout = function(req, res) {

    var accessToken = req.body.access_token;
    var manValues = [accessToken];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);

        }, function(callback) {

            var extraDataNeeded = [];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function(err, userInfo) {

        var userID = userInfo[0].technician_id;

        var sql = "UPDATE `technicians` SET `device_type`=?,`device_token`=? WHERE `technician_id`=? limit 1";
        connection.query(sql, [0, 0, userID], function(err, response) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                var data = {"message": "Logout Successfully"}
                sendResponse.sendSuccessData(data, res);
            }
        });
    }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Access Token Login
 * INPUT : email, password, deviceType, deviceToken, appVersion
 * OUTPUT : user information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.AccessTokenLogin = function(req, res) {

    var accessToken = req.body.access_token;
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    console.log(req.body);

    var manValues = [accessToken, deviceType, appVersion];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {

            var extraDataNeeded = ['first_name', 'last_name', 'mobile', 'image', 'email', 'driver_verified','city','technician_type','is_block'];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);
        },
        function(response, callback) {

            func.checkAppVersion(deviceType, appVersion, res,function(err, updatePopup, critical) {
                callback(null, updatePopup, critical, response);
            });

        }], function(err, updatePopup, critical, response) {

        var userID = response[0].technician_id;
        var loginTime = new Date();
        var city = response[0].city;
        var techType = response[0].technician_type;
            if(response[0].is_block == 1){
                var data = "Your account is blocked. Contact Admin for further details.";
                sendResponse.sendError(data, res);
            }
            else if(response[0].is_block == 2){
                var data = "Your account is deleted. Contact Admin for further details.";
                sendResponse.sendError(data, res);
            }
        else if (response[0].driver_verified == 0)
        {
            var data = {"message": "Thanks for registration. Please wait till Admin approves your request."};
            sendResponse.sendSuccessData(data, res);
        }
        else
        {
            func.updateUserParams(deviceToken, deviceType, loginTime, appVersion, userID);

            var data = {access_token: accessToken, update_popup: updatePopup, critical: critical, first_name: response[0].first_name, last_name: response[0].last_name, mobile: response[0].mobile, image: response[0].image, user_id: userID, email: response[0].email, city:city,technician_type:techType};
            func.GetCardsDataOfTech(userID, function(cards) {
                func.HomeScreenData(userID, function (services) {
                    data.home_data = services;
data.cards = cards;
                    sendResponse.sendSuccessData(data, res);
                });
            });
        }
    }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Access Token Login
 * INPUT : email, password, deviceType, deviceToken, appVersion
 * OUTPUT : user information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.GetHomescreenData = function(req, res) {

    var accessToken = req.body.access_token;

    console.log(req.body);

    var manValues = [accessToken];

    async.waterfall([
            function(callback) {
                func.checkBlankWithCallback(res, manValues, callback);
            },
            function(callback) {

                var extraDataNeeded = [];
                func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);
            }],
            function(err, response) {


                var userID = response[0].technician_id;

                func.HomeScreenData(userID, function (services) {
                    var data = {home_data: services};
                    sendResponse.sendSuccessData(data, res);
                });
            }

    );

};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Authenticate fb user
 * INPUT : fbId,fbAccessToken
 * OUTPUT : Authenticated or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function checkFbUser(res, updatePopup, critical, userFbId, fbAccessToken, callback) {

    var request = require('request');
    var urls = "https://graph.facebook.com/" + userFbId + "?fields=updated_time&access_token=" + fbAccessToken;

    request(urls, function(error, response, body) {
        if (!error && response.statusCode == 200) {

            var output = JSON.parse(body);
            if (output['error']) {
                var error = 'Not an authenticated user.';
                sendResponse.sendError(error, res);
            }
            else {
                var sql = "SELECT `technician_id`, `access_token`, `first_name`, `last_name`, `email`, `image`, `mobile`, `fb_id`,`driver_verified`,`city`,`technician_type`,`is_block` FROM `technicians` WHERE `fb_id` = ? LIMIT 1";
                connection.query(sql, [userFbId], function(err, userResponse) {

                    if (err) {
                        console.log(err);
                        sendResponse.somethingWentWrongError(res);

                    }

                    else
                    {

                        return callback(null, updatePopup, critical, userResponse);
                    }

                });
            }
        }
        else {
            var error = 'Not an authenticated user.';
            sendResponse.sendError(error, res);
        }
    });
}


function getUserIDFromEmail(res, email, callback) {

    var sql = "SELECT `technician_id` FROM `technicians` WHERE `email`=? && `is_block`=? LIMIT 1";
    connection.query(sql, [email,0], function(err, responseUser) {

        if (responseUser.length == 0) {
            var error = 'Email not registered.';
            sendResponse.sendError(error, res);
        }
        else {

            var userID = responseUser[0].technician_id;
            callback(null, userID);
        }
    });
}

function forgotPasswordRequest(email, link, callback) {

    var to = email;
    var sub = "[Vizavoo] Please reset your password";
    //var link1 = "api.vizavoo.com/customer_password/forget_password.php?token="+link;
    var msg = "Hi, <br><br>";
    msg += "We have received a password change request for your account.<br>";
    msg += "If you made this request, then";
    msg += "<a href = "+genVarSettings.domainName+"/technician_password/forget_password.php?token=" + link + "> click this link to reset your password. </a><br>";
    msg += "If you did not ask to change your password, then please ignore this email. Another user may have entered your email by mistake. No changes will be made to your account. <br><br>";
    msg += "Thanks,<br>Vizavoo Team. <br>";

    func.sendEmail(to, msg, sub, function(result) {
        return callback(result);
    });
}


/*
 * ------------------------------------------------------
 *  Check link
 * Input: token
 * Output: token valid or not
 * ------------------------------------------------------
 */
exports.CheckTokenFromLink = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Content-Type', 'application/json');
    var token = req.body.token;


    var sql = "SELECT technician_id FROM technicians WHERE link_one_time_password=? LIMIT 1";
    connection.query(sql, [token], function(err, response) {
        if (response.length == 1)
        {

            var response1 = {"log": 'Success.'};
            res.send(JSON.stringify(response1));


        }
        else
        {
            var response1 = {"error": 'Invalid token.'};
            res.send(JSON.stringify(response1));
        }
    });


};

/*
 * ------------------------------------------------------
 *  set password
 * Input: email, password
 * Output: password set
 * ------------------------------------------------------
 */
exports.SetPasswordFromUserEmailAndPassword = function(req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Content-Type', 'application/json');
    var token = req.body.token;
    var password = req.body.password;

    var manValues = [token, password];
    var checkData = func.checkBlank(manValues);
    if (checkData == 1)
    {

        var response = {"error": 'Some parameter missing.', "flag": 1};
        res.send(JSON.stringify(response));
    }
    else
    {
        if (password.length < 6) {

            var response1 = {"error": 'Your password must be at least 6 characters long.', "flag": 1};
            res.send(JSON.stringify(response1));
        }
        else{
            
        
        password = md5(password);

        var sql = "UPDATE technicians set password=?,link_one_time_password=?,forgot_password_set = ? WHERE link_one_time_password=? LIMIT 1";
        connection.query(sql, [password, '', 0, token], function(err, responseUpdate) {
            if (err)
            {

                var response1 = {"error": 'Password not set.'};
                res.send(JSON.stringify(response1));

            }
            else
            {
                var response1 = {"log": 'Password set successfully.'};
                res.send(JSON.stringify(response1));
            }

        });
        }
    }

};


function checkEmailPasswordAndUserType(res, updatePopup, critical, email, password, callback) {

    var md5 = require('MD5');
    var encryptPassword = md5(password);

    var sql = "SELECT `technician_id`, `driver_verified`,`access_token`,`first_name`,`last_name`,`mobile`,`image`,`city`,`technician_type`,`is_block` FROM `technicians` WHERE `email`=? and `password`=? LIMIT 1";
    connection.query(sql, [email, encryptPassword], function(err, response) {

        if (!response.length) {
            var error = 'The email or password you entered is incorrect.';
            sendResponse.sendError(error, res);
        }
        else if(response[0].is_block == 1){
            var data =  "Your account is blocked. Contact Admin for further details.";
            sendResponse.sendError(data, res);
        }
        else if(response[0].is_block == 2){
            var data =  "Your account is deleted. Contact Admin for further details.";
            sendResponse.sendError(data, res);
        }
        else if (response[0].driver_verified == 0) {
            var data = {"message": "Thanks for registration. Please wait till Admin approves your request."};
            sendResponse.sendSuccessData(data, res);
        }
        else {
            callback(null, updatePopup, critical, response);
        }
    });
}


function checkEmailForFbRegister(email, callback) {

    var sql = "SELECT `technician_id`, `access_token`,`first_name`,`last_name`,`mobile`,`image`,`driver_verified`,`city`,`technician_type`,`is_block` FROM `technicians` WHERE `email`=?";
    connection.query(sql, [email], function(err, response) {

        return callback(response);
    });
}


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * check Email Availability
 * INPUT : email
 * OUTPUT : email available or not
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function checkEmailAvailability(res, email, callback) {

    var sql = "SELECT `technician_id` FROM `technicians` WHERE `email`=? limit 1";
    connection.query(sql, [email], function(err, response) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {
            var error = 'Email already exists.';
            sendResponse.sendError(error, res);
        }
        else {
            callback();
        }
    });
}


function pad(val, max) {
    var str = val.toString();
    return str.length < max ? pad("0" + str, max) : str;
}