AWSSettings = require('../config/ApplicationSettings').getAWSSettings();
genVarSettings = require('../config/ApplicationSettings').generalVariableSettings();
emailSettings = require('../config/ApplicationSettings').emailSettings();
var sendResponse = require('./sendResponse');

/*
 * ------------------------------------------------------
 * Check if mandatory fields are not filled
 * INPUT : array of field names which need to be mandatory
 * OUTPUT : Error if mandatory fields not filled
 * ------------------------------------------------------
 */

exports.checkBlank = function (arr) {

    return (checkBlank(arr));
};

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '') {
            return 1;
            break;
        }
        else if (arr[i] == undefined) {
            return 1;
            break;
        }
        else if (arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}

exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

/*
 * ------------------------------------------------------
 *  Authenticate a user
 *  INPUT : user_access_token
 *  OUTPUT : user_id,brand_id,email,timezone,currency
 * ------------------------------------------------------
 */
exports.authenticateUser = function (accessToken, callback) {

    var sql = "SELECT `technician_id`, `first_name`, `last_name`, `gender`, `image`, `email`, `fb_id`, `access_token`, `device_type`, `device_token`, `app_version`, `created_at`, `updated_at`, `delete_status` FROM `technicians` WHERE `access_token`=? LIMIT 1";
    connection.query(sql, [accessToken], function (err, result) {

        return callback(result);
    });
};


/*
 * ------------------------------------------------------
 * Authenticate a user through Access token and return extra data
 * Input:Access token{Optional Extra data}
 * Output: User_id Or Json error{Optional Extra data}
 * ------------------------------------------------------
 */
exports.authenticateAccessTokenAndReturnExtraData = function (accesstoken, arr, res, callback) {

    var sql = "SELECT `technician_id`";
    arr.forEach(function (entry) {
        sql += "," + entry;
    });
    sql += " FROM `technicians`";
    sql += " WHERE `access_token`=? LIMIT 1";
    connection.query(sql, [accesstoken], function (err, result) {

        if (err) {
            sendResponse.parameterMissingError(res);
        }
        else if (result.length > 0) {

            return callback(null, result);

        } else {
            sendResponse.invalidAccessTokenError(res);

        }
    });

}


/*
 * ------------------------------------------------------
 *  Get Number Of Cards For The User
 *  INPUT : userID
 *  OUTPUT : number of cards
 * ------------------------------------------------------
 */

exports.GetNumberOfCardsForTheUser = function (userID, res,callback) {

    var sql = "SELECT `card_id` FROM `technician_cards` WHERE `technician_id`=?";
    connection.query(sql, [userID], function (err, response) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            callback(null,userID,response.length);
        }
    });
};
exports.GetCardsDataOfTech = function (userID, callback) {
    var sql = "SELECT `card_id`, `technician_id`, `last_4`, `card_type`, `default_status` FROM `technician_cards` WHERE `technician_id`=?";
    connection.query(sql, [userID], function (err, responseCards) {

            callback(responseCards);

    });
};

/*
 * ------------------------------------------------------
 *  check App Version
 *  INPUT : appVersion
 *  OUTPUT : update popup and critical
 * ------------------------------------------------------
 */

exports.checkAppVersion = function (deviceType, appVersion,res, callback) {

    var sql = "SELECT `id`, `type`, `version`, `critical`,`last_critical` FROM `app_version` WHERE `type`=? && `app_type`=? limit 1";
    connection.query(sql, [deviceType,1], function (err, response) {

        appVersion = parseInt(appVersion);

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (response[0].version > appVersion) {
            var critical = response[0].critical;
            if(response[0].last_critical > appVersion)
                {
                    critical = 1;
                }
            callback(null, 1, critical);
        }
        else {
            callback(null, 0, 0);
        }
    });
};


/*
 * -----------------------------------------------------------------------------
 * Uploading image to s3 bucket
 * INPUT : file parameter
 * OUTPUT : image path
 * -----------------------------------------------------------------------------
 */
function uploadImageToS3Bucket(file, folder, callback) {

    var fs = require('fs');
//    var mathjs = require('mathjs');
//    var math = mathjs();
    var AWS = require('aws-sdk');

    if (file == undefined) {
        return callback("logo.jpeg");
    }
    else {
        var filename = file.name; // actual filename of file
        var path = file.path; //will be put into a temp directory
        var mimeType = file.type;

        fs.readFile(path, function (error, file_buffer) {

            if (error) {
                //  console.log(error)
                return callback("logo.jpeg");
            }
            else {
                var length = 5;
                var str = '';
                var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var size = chars.length;
                for (var i = 0; i < length; i++) {

                    var randomnumber = Math.floor(Math.random() * size);
                    str = chars[randomnumber] + str;
                }

                filename = "vizavoo-" + str + "-" + file.name;
                filename = filename.split(' ').join('-');

                AWS.config.update({accessKeyId: AWSSettings.awsAccessKey, secretAccessKey: AWSSettings.awsSecretKey});
                var s3bucket = new AWS.S3();
                var params = {Bucket: AWSSettings.awsBucket, Key: folder + '/' + filename, Body: file_buffer, ACL: 'public-read', ContentType: mimeType};

                s3bucket.putObject(params, function (err, data) {
                    if (err) {
                        //  console.log(err)
                        return callback("logo.jpeg");
                    }
                    else {
                        return callback(filename);
                    }
                });
            }
        });
    }
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Update user parameters
 * INPUT : deviceToken, deviceType, loginTime, appVersion, userID
 * OUTPUT : Update the params
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.updateUserParams = function(deviceToken, deviceType, loginTime, appVersion, userID) {

    var sql = "UPDATE `technicians` SET `device_type`=?,`device_token`=?,`app_version`=?,`updated_at`=? WHERE `technician_id`=? limit 1";
    connection.query(sql, [deviceType, deviceToken, appVersion, loginTime, userID], function(err, result) {
    });
};
/*
 * -----------------------------------------------------------------------------
 * sorting an array in ascending order
 * INPUT : array and key according to which sorting is to be done
 * OUTPUT : sorted array
 * -----------------------------------------------------------------------------
 */
exports.sortByKeyAsc = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
};
/*
 * -----------------------------------------------------------------------------
 * sorting an array in descending order
 * INPUT : array and key according to which sorting is to be done
 * OUTPUT : sorted array
 * -----------------------------------------------------------------------------
 */
exports.sortByKeyDesc = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
};



/*
 * -----------------------------------------------------------------------------
 * Encryption code
 * INPUT : string
 * OUTPUT : crypted string
 * -----------------------------------------------------------------------------
 */
exports.encrypt = function (text) {

    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}


exports.sendEmail = function (receiverMailId, message, subject, callback) {
    var nodemailer = require("nodemailer");
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: emailSettings.username,
            pass: emailSettings.password
        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: "VizaVoo <"+emailSettings.username+">", // sender address
        to: receiverMailId, // list of receivers
        subject: subject, // Subject line
        html: message // plaintext body
    }

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            return callback(0);
        } else {
            return callback(1);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
};

exports.sendFeedbackEmail = function (senderEmail, message, subject, callback) {

    var nodemailer = require("nodemailer");
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: emailSettings.username,
            pass: emailSettings.password
        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: senderEmail, // sender address
        to: emailSettings.feedback_mail, // list of receivers
        replyTo: senderEmail,
        subject: subject, // Subject line
        text: message // plaintext body
    }

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (err, response) {
        if (err) {
            return callback(err);
        } else {
            return callback(null,1);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
};


exports.generateRandomString = function () {
    var mathjs = require('mathjs');
    math = mathjs();
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 6; i++)
        text += possible.charAt(math.floor(math.random() * possible.length));

    return text;
};

exports.getDateTime = function (date,flag) {

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day = date.getDate();
    day = (day < 10 ? "0" : "") + day;
if(flag == 0){
    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
}
else{
    return year + "-" + month + "-" + day + " " + hour + ":" + min;
}

};


exports.getImageNameAfterUpload = function (file, folder, callback) {

    if ((file) && (file.name)) {
        uploadImageToS3Bucket(file, folder, function (fileName) {
            return callback(fileName);
        });
    }
    else {
        return callback("logo.jpeg")
    }
};

/*
 * -----------------------------------------------------------------------------
 * return time difference of cur date and another time
 * INPUT : time
 * OUTPUT : diff of 2 times
 * -----------------------------------------------------------------------------
 */
exports.getDaysDifference = function(time, flag)
{
    //var Math = require('mathjs');
    var today = new Date();
    if (flag == 1)
    {
        var diffMs = (time - today); // milliseconds between now & post date
    }
    else
    {
        var diffMs = (today - time); // milliseconds between now & post date
    }
    var diffDays = Math.floor(diffMs / 86400000); // days
//    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
//    var diffMins = Math.floor(((diffMs % 86400000) % 3600000) / 60000); // minutes
//    var diffSecs = Math.floor((((diffMs % 86400000) % 3600000) % 60000)/1000); // seconds
//    var postTime = {"days": diffDays, "hours": diffHrs, "minutes": diffMins,"seconds":diffSecs};

    return diffDays;

};


/*
 * -----------------------------------------------------------------------------
 * Sending push notification to devices
 * INPUT : iosDeviceToken,message
 * OUTPUT : Notification send
 * -----------------------------------------------------------------------------
 */
exports.sendApplePushNotification = function(iosDeviceToken, message)
{
    var apns = require('apn');
    var deviceCount = iosDeviceToken.length;
    if(deviceCount != 0)
        {
            
    var options = {
        cert: genVarSettings.pemFile,
        certData: null,
        key: genVarSettings.pemFile,
        keyData: null,
        passphrase: 'click',
        ca: null,
        pfx: null,
        pfxData: null,
        gateway: genVarSettings.iosGateway,
        port: 2195,
        rejectUnauthorized: true,
        enhanced: true,
        cacheLength: 100,
        autoAdjustCache: true,
        connectionTimeout: 0,
        ssl: true
    }
    
    for(var i=0; i<deviceCount; i++)
        {
             var deviceToken1 = iosDeviceToken[i].replace(/[^0-9a-f]/gi, "");

    if((deviceToken1.length) % 2)
    {
        console.log("error")
    }
    else
    {
    var deviceToken = new apns.Device(iosDeviceToken[i]);
    var apnsConnection = new apns.Connection(options);
    var note = new apns.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600;
    note.badge = 0;
    note.sound = 'ping.aiff';
    note.alert = message;
    //note.payload  = {"event_id": eventId, "flag": flag};
    apnsConnection.pushNotification(note, deviceToken);

// i handle these events to confirm the notification gets
// transmitted to the APN server or find error if any

    function log(type) {
        return function() {
            console.log(type, arguments);
        }
    }

    apnsConnection.on('transmitted', function() {


    });

    apnsConnection.on('error', log('error'));
    apnsConnection.on('transmitted', log('transmitted'));
    apnsConnection.on('timeout', log('timeout'));
    apnsConnection.on('connected', log('connected'));
    apnsConnection.on('disconnected', log('disconnected'));
    apnsConnection.on('socketError', log('socketError'));
    apnsConnection.on('transmissionError', log('transmissionError'));
    apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));
    }
        }
        }
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * sendIosPushNotification
 * INPUT : deviceToken, message
 *
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */


exports.sendAndroidPushNotification = function(resulTokens,text)
{
    var gcm = require('node-gcm');


    //var message = new gcm.Message({
    //    collapseKey: 'demo',
    //    delayWhileIdle: false,
    //    timeToLive: 2419200,
    //    data: {
    //        message: message,
    //        brand_name: "Jugnoo"
    //    }
    //});



    var message = new gcm.Message({
        
        collapseKey: 'demo',
        delayWhileIdle: true,
         data: {
            message: text
        }
    });
    var sender = new gcm.Sender(genVarSettings.androidKey);
     var registrationIds = resulTokens;
     //registrationIds.push(resulTokens)
     console.log(message)
    sender.sendNoRetry(message, registrationIds, function(err, result) {
                                            console.log(err);
                                            console.log(result);
    });
}

exports.getDateOnly = function (date) {

    var dd = date.getDate();
    var mm = date.getMonth()+1; //January is 0!

    var yyyy = date.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    return  yyyy + "-" + mm + "-" + dd;
    
};

/*
 * -----------------------------------------------------------------------------
 * return time difference of cur date and another time
 * INPUT : time
 * OUTPUT : diff of 2 times
 * -----------------------------------------------------------------------------
 */
exports.getTimeDifference = function(time, flag)
{

    var today = new Date();

    if (flag == 1)
    {
        var diffMs = (time - today); // milliseconds between post date & now
    }
    else
    {
        var diffMs = (today - time); // milliseconds between now & post date
    }

    var seconds = Math.floor(0.001 * diffMs);  // in seconds

    return seconds;

};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * HomeScreenData
 * OUTPUT : Update the params
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.HomeScreenData = function(userId, callback) {
    var curDate = new Date();
    var sql = "SELECT count(`id`) as new_bookings FROM `booking_requests` WHERE `technician_id`=? and `status`=? and `booking_datetime` > ?";
    connection.query(sql, [userId, 0, curDate], function(err, newBookings) {

        var sql = "SELECT count(`id`) as upcoming_bookings FROM `booking_timings` WHERE `technician_id`=? and `start_time` > ? and (`status` = ? || `status`=?)";
        connection.query(sql, [userId, curDate, 1,7], function(err, upcomingBookings) {

            var sql = "SELECT  a.`id`,b.`name`,a.`address`,a.`latitude`,a.`longitude`,a.`start_time`,a.`local_start_time`,a.`service_price`,a.`service_type`," +
                "a.`service_time`,b.`image`, c.`first_name`, c.`last_name`, c.`mobile` FROM `booking_timings` a, `service` b, `users` c WHERE " +
                "a.`technician_id`=? && a.`start_time`>? && a.`service_id`=b.`service_id` && a.`user_id`=c.`user_id` && a.`status` = ? ORDER BY a.`start_time` ASC LIMIT 1";
            connection.query(sql, [userId, curDate, 1], function(err, responseUpcoming) {

                if (responseUpcoming.length > 0)
                {
                    var startTime = new Date(responseUpcoming[0].start_time);
                    var diffMs = (startTime - curDate);
                    var outToServeFlag = 0;
                    if(diffMs < 7200000){
                        outToServeFlag = 1;
                    }
                    responseUpcoming[0].local_start_time = responseUpcoming[0].local_start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    responseUpcoming[0].image = servicePicBaseUrl + responseUpcoming[0].image;
                    responseUpcoming[0].out_to_serve_flag = outToServeFlag;
                }

                var services = {new_booking_count: newBookings[0].new_bookings, upcoming_booking_count: upcomingBookings[0].upcoming_bookings, "upcoming": responseUpcoming};

                return callback(services);

            });
        });
    });
}