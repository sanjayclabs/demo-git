var func = require('./commonfunction');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');
var generatePassword = require('password-generator');

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get Profile Data
 * INPUT : access_token
 * OUTPUT : profile information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.GetProfileData = function(req, res) {

    var accessToken = req.body.access_token;

    console.log(req.body);
    var manValues = [accessToken];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {

            var extraDataNeeded = ['email', 'mobile', 'image', 'first_name', 'last_name'];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function(err, userInfo) {

        userInfo[0].image = profilePicBaseUrl + userInfo[0].image

        sendResponse.sendSuccessData(userInfo, res);
    }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * edit Profile Data
 * INPUT : access_token
 * OUTPUT : profile information
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.EditProfileData = function(req, res) {

    var accessToken = req.body.access_token;
    
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var mobile = req.body.mobile;
    var city = req.body.city;
    var techType = req.body.tech_type;

    console.log(req.body);
    console.log(req.files);
    var manValues = [accessToken, mobile, firstName];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {
            var extraDataNeeded = ['image'];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }, function(userInfo, callback) {

            if ((req.files) && (req.files.profile_pic) && (req.files.profile_pic.name)) {

                func.getImageNameAfterUpload(req.files.profile_pic, "profile", function(imageName) {
                    callback(userInfo, profilePicBaseUrl+imageName);
                });
            }
            else {
                callback(userInfo, userInfo[0].image);
            }

        }], function(userInfo, imageName) {

        var userID = userInfo[0].technician_id;
           
//        var sql = "SELECT `technician_id` FROM `technicians` WHERE `email`=? LIMIT 1";
//        connection.query(sql, [email], function(err, response1) {
//
//            if ((response1.length > 1) || (response1[0].user_id != userID))
//            {
//                var error = 'Email Already in use.';
//                sendResponse.sendError(error, res);
//            }
//            else
//            {
                var sql = "UPDATE `technicians` SET `image`=?,`mobile`=?,`first_name`=?,`last_name`=?, `city`=?,`technician_type`=? WHERE `technician_id`=? limit 1";
                connection.query(sql, [ imageName, mobile, firstName, lastName,city,techType, userID], function(err, response) {

                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var data = {"message": "Profile Updated Successfully.","image":imageName}
                        sendResponse.sendSuccessData(data, res);
                    }
                });
           // }
        //});
    }
    );
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get feedback by customer
 * INPUT : access_token, message
 * OUTPUT : feedback submitted successfully
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.FeedbackByTechnician = function(req, res) {

    var accessToken = req.body.access_token;
    var message = req.body.message;

    console.log(req.body);
    var manValues = [accessToken, message];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {

            var extraDataNeeded = ['email', 'first_name', 'last_name'];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function(err, userInfo) {

        var from = userInfo[0].email;
            var techId = userInfo[0].technician_id;
        var sub = "[Vizavoo Technician] Feedback by " + userInfo[0].first_name;

        func.sendFeedbackEmail(from, message, sub, function(err, result) {

            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result) {
                var sql = "INSERT INTO `technician_feedback`(`technician_id`, `feedback_text`) VALUES (?,?)";
                connection.query(sql, [techId, message], function(err, response) {
                });
                    var data = {"message": "Feedback Sent Successfully."}
                sendResponse.sendSuccessData(data, res);

            }
            else {
                sendResponse.somethingWentWrongError(res);

            }
        });
    }
    );
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * change password of the user
 * INPUT : access_token, oldPassword, newPassword
 * OUTPUT : password of user changed
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.ChangePasswordOfUser = function(req, res) {

    var accessToken = req.body.access_token;
    var oldPassword = req.body.old_password;
    var newPassword = req.body.new_password;

    console.log(req.body);
    var manValues = [accessToken, oldPassword, newPassword];

    async.waterfall([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function(callback) {

            var extraDataNeeded = ['password'];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function(err, userInfo) {

        oldPassword = md5(oldPassword);
        if (oldPassword != userInfo[0].password) {

            var error = 'Incorrect Old Password.';
            sendResponse.sendError(error, res);
        }
        else {
            newPassword = md5(newPassword);
            var userID = userInfo[0].technician_id;
            var sql = "UPDATE `technicians` SET `password`=? WHERE `technician_id`=? limit 1";
            connection.query(sql, [newPassword, userID], function(err, response) {

                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var data = {"message": "Password Updated Successfully."}
                    sendResponse.sendSuccessData(data, res);
                }
            });
        }

    }
    );
};