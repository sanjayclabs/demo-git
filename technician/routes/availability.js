var func = require('./commonfunction');
var sendResponse = require('./sendResponse');
var async = require('async');
/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * This function is used for creating dynamic table for scheduling based on interval
 * Input : interval (e.g. 15 minutes/ 30 minutes/ 45 minutes/ 60 minutes)
 * Output : success log or error
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.CreatingDynamicTableForScheduling = function (req, res) {
    var interval = req.body.interval;

    var offset = (60 / interval);
    var totalColunms = (24 * offset);
    var intervalArray = [];
    for (var i = 0; i < totalColunms; i++) {
        var now = new Date();
        now.setHours(0 + parseInt(i / offset));
        now.setMinutes((i % offset) * interval);
        var dynamicIntervals = pad(now.getHours(), 2) + ':' + pad(now.getMinutes(), 2);
        var intervalString = '`' + dynamicIntervals + '` BOOLEAN NOT NULL DEFAULT 0';
        intervalArray.push(intervalString)
    }
    console.log(intervalArray);

    var sql = "CREATE TABLE IF NOT EXISTS `availability` (`id` INTEGER NOT NULL auto_increment , `user_id` INTEGER NOT NULL, `schedule_date` DATE NOT NULL," + intervalArray + ', PRIMARY KEY(id))';
    connection.query(sql, function (err, responseFromInsertion) {
        if (err) {
            res.type('json');
            res.jsonp(err);
        }
        else {
            var response = {log: 'success'};
            res.type('json');
            res.jsonp(response);
        }
    });

};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Get availability of technician
 * INPUT : accessToken
 * OUTPUT : availability details fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.GetAvailabilityOfTech = function (req, res) {

    var accessToken = req.body.access_token;

    var manValues = [accessToken];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {

            var extraDataNeeded = [];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function (err, userInfo) {
        var technicianId = userInfo[0].technician_id;
        getTechnicianAvailabilty(technicianId, res, function (availabilityData) {
            console.log(availabilityData);
            //var data = {"technician_schedule": availabilityData}
            sendResponse.sendSuccessData(availabilityData, res);
        });

    });
};

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Fetch availability data of tech from table
 * INPUT : techId
 * OUTPUT : availability data fetched
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
function getTechnicianAvailabilty(technicianId, res, callback) {
    var sql = "SELECT `schedule_date`,`00:00`, `00:30`, `01:00`, `01:30`, `02:00`, `02:30`, `03:00`, `03:30`, `04:00`, `04:30`, `05:00`, `05:30`, `06:00`, `06:30`, `07:00`,\n\
 `07:30`, `08:00`, `08:30`, `09:00`, `09:30`, `10:00`, `10:30`, `11:00`, `11:30`, `12:00`, `12:30`, `13:00`, `13:30`, `14:00`, `14:30`, `15:00`, `15:30`, `16:00`, `16:30`, `17:00`,\n\
 `17:30`, `18:00`, `18:30`, `19:00`, `19:30`, `20:00`, `20:30`, `21:00`, `21:30`, `22:00`, `22:30`, `23:00`, `23:30` FROM `availability` WHERE `technician_id` = ?";
    connection.query(sql, [technicianId], function (err, technicianScheduleRespose) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        } else {

            var scheduleLength = technicianScheduleRespose.length;
            var finalArray = [];

            // var userData = [];
            for (var i = 0; i < scheduleLength; i++) {
                var scheduleDateArray = (technicianScheduleRespose[i].schedule_date.toISOString().replace(/T/, ' ').replace(/\..+/, '')).split(' ');
                delete technicianScheduleRespose[i].schedule_date;
                //delete technicianScheduleRespose[i].technician_id;
                // delete technicianScheduleRespose[i].id;
                //var j = 1;
                //var string;
                var booleanArray = new Array();
                //var hoursArray = [];
                for (var key in technicianScheduleRespose[i]) {
                    if (technicianScheduleRespose[i][key] == 0 || technicianScheduleRespose[i][key] == 1) {
                        console.log(key);
                        if (technicianScheduleRespose[i][key] == 1) {
                            technicianScheduleRespose[i][key] = 1;
                        }
                        else {
                            technicianScheduleRespose[i][key] = 0;
                        }
                        booleanArray.push(technicianScheduleRespose[i][key]);
                        //if (j % 2 == 0) {


                        //var booleanArray = new Array();
                        //}
                    }


                    //++j;
                }
                //hoursArray.push(booleanArray);
                finalArray.push({schedule_date: scheduleDateArray[0], hours: booleanArray});
            }
            //userData.technician_schedule = finalArray;
            return callback(finalArray);

        }
    });
}

/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Shift availability of tech by day
 * INPUT : cron that run once in a day
 * OUTPUT : availability copied by one day
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.ShiftAvailabilityDataOfTech = function (req, res) {

    var startDate = new Date();
    startDate.setDate(startDate.getDate() - 1);
    startDate = func.getDateOnly(startDate);

    var endDate = new Date();
    endDate.setDate(endDate.getDate() + 14);
    endDate = func.getDateOnly(endDate);
    console.log('startDate' + startDate);
    console.log('endDate' + endDate);

    var sql = "UPDATE `availability` SET `schedule_date`=? WHERE `schedule_date`=?";
    connection.query(sql, [endDate, startDate], function (err, response) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else {
            var data = {"message": "Done."};
            sendResponse.sendSuccessData(data, res);
        }
    });
};


/*
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * Edit availability of tech
 * INPUT : accessToken, scheduleJson
 * OUTPUT : availability changed
 * ----------------------------------------------------------------------------------------------------------------------------------------
 */
exports.EditAvailabilityOfTech = function (req, res) {

    var accessToken = req.body.access_token;
    var scheduleJson = req.body.schedule_json;

    var manValues = [accessToken, scheduleJson];
    console.log(scheduleJson);
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {

            var extraDataNeeded = [];
            func.authenticateAccessTokenAndReturnExtraData(accessToken, extraDataNeeded, res, callback);

        }], function (err, userInfo) {
        var technicianId = userInfo[0].technician_id;


        var jsonData = JSON.parse(scheduleJson);
        jsonData = jsonData.data;
        var availabiltyLength = jsonData.length;
        var finalArray = [];


        for (var i = 0; i < availabiltyLength; i++) {
            var count = 0;
            var updatedData = '(' + '"' + +technicianId + '","' + jsonData[i]['schedule_date'] + '", ';
            var dynamicColumns = '';
            var totalHours = jsonData[i].hours.length;
            for (var j = 0; j < totalHours; j++) {
                var innerHourArrayLength = 2;  // 30 minutes interval, so in one hour it will occur 2 times.
                // for (var k = 0; k < innerHourArrayLength; k++) {
                var now = new Date();
                now.setHours(0 + parseInt(count / innerHourArrayLength));
                now.setMinutes((count % innerHourArrayLength) * 30);
                var dynamicIntervals = pad(now.getHours(), 2) + ':' + pad(now.getMinutes(), 2);
                count++;
                if (jsonData[i].hours[j] == 1) {
                    var booleanValue = 1;
                }
                else {
                    var booleanValue = 0;
                }
                dynamicColumns += '`' + dynamicIntervals + '`,';
                updatedData += booleanValue + ',';
                // }
            }
            updatedData = updatedData.substring(0, updatedData.length - 1);
            updatedData += ')';
            finalArray.push(updatedData);
        }


        dynamicColumns = dynamicColumns.substring(0, dynamicColumns.length - 1);

        var sql = "DELETE FROM `availability` WHERE `technician_id` = ?";
        connection.query(sql, [technicianId], function (err, resultDelete) {
            //console.log(finalArray);
            var sql = "INSERT INTO `availability`(`technician_id`,`schedule_date`," + dynamicColumns + ") VALUES " + finalArray;
            connection.query(sql, function (err, resultInsert) {
                if (err)
                    console.log(err);

                var data = {"message": "Availability calendar updated successfully."};
                sendResponse.sendSuccessData(data, res);
            });


        });


    });
};

// provide padding to time format
function pad(val, max) {
    var str = val.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

